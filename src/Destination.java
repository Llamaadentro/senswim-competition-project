import com.google.api.client.http.FileContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.DimensionProperties;
import com.google.api.services.sheets.v4.model.ValueRange;

import java.awt.*;
import java.io.*;

import com.google.gdata.data.analytics.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.sun.org.apache.xpath.internal.SourceTree;
import jxl.read.biff.BlankCell;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.Color;
//import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.util.CellRangeAddress;
//import sun.misc.IOUtils;
import org.apache.poi.util.IOUtils;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xwpf.usermodel.VerticalAlign;


import java.lang.reflect.Array;
import java.net.URL;
import java.util.List;

import com.itextpdf.text.pdf.BidiLine;
import com.itextpdf.text.pdf.PdfWriter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


public class Destination {

    String SSID;
    int poollength;
    int lanes;
    String competitionname;
    String poolname;
    String date;
    Date warmupstarttime;
    Date openingtime;
    Date compstarttime;
    int eventinterval;
    int heatinterval;
    int awardlength;
    int awardfrequency;
    String pictureadress;
    String timetableshortlink;
    String timetablefulllink;
    TreeMap<Integer, Integer> heattimes;

    ArrayList<Event> fulleventlist;
    ArrayList<Event> realeventlist;
    ArrayList<Event> awardlist;
    TreeMap<Integer, Event> eventmap;
    TreeMap<String, Swimmer> swimmerdb;
    TreeMap<String, Swimmer> relayteamlist;

    public Destination(String SSID) throws IOException, ParseException {

        this.heattimes = new TreeMap<Integer, Integer>();
        heattimes.put(50, 1);
        heattimes.put(100, 2);
        heattimes.put(200, 3);
        heattimes.put(400, 6);
        heattimes.put(800, 11);
        heattimes.put(1500, 22);

        this.SSID = SSID;
        fulleventlist = new ArrayList<Event>();
        realeventlist = new ArrayList<Event>();
        awardlist = new ArrayList<Event>();
        swimmerdb = new TreeMap<String, Swimmer>();
        relayteamlist  = new TreeMap<String, Swimmer>();
        eventmap  = new TreeMap<Integer, Event>();
        //���� ���������� � �������� � ������������

        Sheets service = QuickStart.getSheetsService();
        ValueRange response = service.spreadsheets().values().get(this.SSID, "Planning2!A2:I4").execute();
        List<List<Object>> data = response.getValues();

        if (data == null || data.size() == 0) {
            System.out.println("No data found.");
        } else {
            int rowcounter = 0;
            for (List row : data) {

                if (rowcounter == 0) {

                   if(row.size()>0) {
                       this.competitionname = (String) row.get(0);
                   } else {
                       this.competitionname = "New competition";
                   }
                } else if (rowcounter == 1) {
                    if(((String)row.get(1)).equals("")) {
                        this.poolname = "New pool";
                    } else {
                        this.poolname = (String) row.get(1);
                    }

                    if(((String)row.get(7)).equals("")) {
                        this.date = LocalDate.now().getDayOfMonth()+"."+LocalDate.now().getMonth()+"."+LocalDate.now().getYear();
                    } else {
                        this.date = (String) row.get(7);
                    }
                } else if (rowcounter == 2) {
                    if(((String)row.get(4)).equals("")) {
                        this.poollength = 50;
                    } else {
                        this.poollength = Integer.parseInt((String) row.get(4));
                    }
                    if(((String)row.get(8)).equals("")) {
                        this.lanes = 3;
                    } else {
                        this.lanes = Integer.parseInt((String) row.get(8));
                    }
                }
                rowcounter++;
            }
        }

        //���� ���������� � �������������

        ValueRange advanced = service.spreadsheets().values().get(this.SSID, "Advanced settings!A7:C16").execute();
        List<List<Object>> advancedsettings = advanced.getValues();

        if (advancedsettings == null || advancedsettings.size() == 0) {
            System.out.println("No data found.");
        } else {
            int rowcounter1 = 0;
            String wust ="";
            String ocst = "";
            String cst = "";
            for (List row : advancedsettings) {

                if (rowcounter1 == 0) {

                    if(row.size()>2) {
                        this.awardfrequency = Integer.parseInt((String) row.get(2));
                    } else {
                        this.awardfrequency = 1000;
                    }
                } else if (rowcounter1 == 2) {

                    if(row.size()>2) {
                        wust = (String) row.get(2);
                        //������ ����������� �������
                        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
                        Date date = df.parse(wust);
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(date);
                        this.warmupstarttime = cal.getTime();
                    }

                } else if (rowcounter1 == 3) {

                    if(row.size()>2) {
                        ocst = (String) row.get(2);
                        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
                        Date date = df.parse(ocst);
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(date);
                        this.openingtime = cal.getTime();
                    }

                } else if (rowcounter1 == 4) {
                    if(row.size()>2) {
                        cst = (String) row.get(2);
                    } else {
                        cst = "10:00";
                    }
                        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
                        Date date = df.parse(cst);
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(date);
                        this.compstarttime = cal.getTime();
                        if (wust.equals("")){
                            this.warmupstarttime = this.compstarttime;
                        }
                        if (ocst.equals("")){
                            this.openingtime = this.compstarttime;
                        }

                } else if (rowcounter1 == 5) {
                    if(row.size()>2) {
                        this.awardlength = Integer.parseInt((String) row.get(2));
                    } else {
                        this.awardlength = 10;

                    }
                } else if (rowcounter1 == 6) {
                    if(row.size()>2) {
                        this.eventinterval = Integer.parseInt((String) row.get(2));
                    } else {
                        this.eventinterval = 0;
                    }
                } else if (rowcounter1 == 7) {
                    if (row.size() > 2) {
                        this.heatinterval = Integer.parseInt((String) row.get(2));
                    } else {
                        this.heatinterval = 0;
                    }
                } else if (rowcounter1 == 9) {
                    if(row.size()>0) {
                        this.pictureadress = (String) row.get(0);
                    } else {
                        this.pictureadress = "https://pp.userapi.com/c845016/v845016467/18a573/jsqiy6MfBoI.jpg";
                    }
                }
                rowcounter1++;
            }
        }
//JSON�������� ����� ���������� � ����� ����������

        JSONObject destinationjson = new JSONObject();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");

        destinationjson.put("name", this.competitionname);
        destinationjson.put("place", this.poolname);
        destinationjson.put("date", this.date);
        destinationjson.put("lanes", this.lanes);
        destinationjson.put("poollength", this.poollength);
        destinationjson.put("picture", this.pictureadress);
        destinationjson.put("start",df.format(this.warmupstarttime));

        String finale = destinationjson.toJSONString();

        try(FileWriter file = new FileWriter("file1.txt")){
            file.write(finale);
            System.out.println("Object: "+ destinationjson);
        }

//        return Response.ok(finale, MediaType.APPLICATION_JSON).build();

    }

    //����������� � �������� ��� ������ �� ��������� ������ � ��������������� �������� �� ����
    public Destination(String competitionname, String day, String poolname, String starttime, int poollength, int lanes, String picture)throws IOException, ParseException{
        String cst = starttime;
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        Date date = df.parse(cst);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        this.compstarttime = cal.getTime();
        this.warmupstarttime = compstarttime;
        this.openingtime = compstarttime;

        this.awardfrequency = 1000;
        this.competitionname = competitionname;
        this.date = day;
        this.poolname = poolname;
        this.poollength = poollength;
        this.lanes = lanes;
        this.pictureadress = picture;
        
        this.timetableshortlink = "";
        this.timetablefulllink = "";
        
        this.eventinterval = 0;
        this.heatinterval = 0;

        this.heattimes = new TreeMap<Integer, Integer>();
        heattimes.put(50, 1);
        heattimes.put(100, 2);
        heattimes.put(200, 3);
        heattimes.put(400, 6);
        heattimes.put(800, 11);
        heattimes.put(1500, 22);
        realeventlist = new ArrayList<Event>();

        awardlist = new ArrayList<Event>();
        swimmerdb = new TreeMap<String, Swimmer>();
        relayteamlist = new TreeMap<String, Swimmer>();
        eventmap  = new TreeMap<Integer, Event>();

        System.out.println(Colors.CYAN+"List the files to read :"+Colors.RESET);
        Scanner filelist = new Scanner(System.in);
        String[] files = new String[3];
        ArrayList<String[]> headers = new ArrayList<>();

        for (int i = 0; i <3 ; i++) {
            files[i] = filelist.nextLine();

            try{ if (!(files[i].substring(files[i].length()-3,files[i].length()).equals("txt"))) { throw new Exception("Wrong file format"); }
            try  {
                FileInputStream swim = new FileInputStream(files[i]);
                BufferedReader swimreader = new BufferedReader(new InputStreamReader(swim));
                    String headstring = swimreader.readLine();
                    headers.add(headstring.split(";"));

            } catch (Exception ex){ System.out.println(ex.getMessage()); }
        } catch(Exception ex) { System.out.println(ex.getMessage()); }
     }

//        for (String[] mass: headers) {
//            System.out.println(Arrays.toString(mass) +", length "+ mass.length);
//        }

        //�������� ���� ������ �������
        for (int i = 0; i <3 ; i++) {
            if (headers.get(i).length == 7){
                try  {
                    FileInputStream swim = new FileInputStream(files[i]);
                    BufferedReader swimreader = new BufferedReader(new InputStreamReader(swim, "ISO-8859-8"));
                    Scanner swimscan = new Scanner(swimreader);
                        int linecounter = 0;

                        while (swimscan.hasNextLine()){

                                String[] data = swimscan.nextLine().split(";");
                                if(linecounter>0) {

                                if( data[0].length()<8){
                                    Swimmer guy = new Swimmer("",data[2].substring(1,data[2].length()-1),"", data[5].substring(1,data[5].length()-1));
                                    relayteamlist.put(data[0],guy);
                                } else {
                                    Swimmer guy = new Swimmer(data[3].substring(1,data[3].length()-1), data[2].substring(1,data[2].length()-1), data[4].substring(1,data[4].length()-1), data[5].substring(1,data[5].length()-1));
                                    swimmerdb.put(data[0],guy);
                                }
                            }

                         linecounter++;
                        }

                } catch (Exception ex){ ex.getMessage(); }
            }
        }
        //�������� ������ ������ �������
        for (int i = 0; i <3 ; i++) {
            if (headers.get(i).length == 8){
                try  {
                    FileInputStream swim = new FileInputStream(files[i]);
                    BufferedReader swimreader = new BufferedReader(new InputStreamReader(swim, "UTF-8"));
                    Scanner swimscan = new Scanner(swimreader);
                    int linecounter = 0;
                    while (swimscan.hasNextLine()){
                            String[] data = swimscan.nextLine().split(";");
                                  if ((!data[0].equals(""))  && linecounter>0 ){

                            Event event = new Event();
                            String dist = data[3];
                            String styl = data[4];
                            String gen = data[5].substring(1, 2);

                            switch (dist) {
                                case "0": event.distance = 50; break;
                                case "1": event.distance = 100; break;
                                case "2": event.distance = 200; break;
                                case "3": event.distance = 400; break;
                                case "4": event.distance = 800; break;
                                case "5": event.distance = 1500; break;
                                case "8": event.distance = 200; break;
                                case "9": event.distance = 400; break;
                                case "10": event.distance = 800; break;
                                default: event.distance = 100;
                            }
                            switch (styl) {
                                case "0": if(gen.equals("R")){event.stroke = "Relay";} else { event.stroke = "Freestyle";} break;
                                case "1": if(gen.equals("R")){event.stroke = "Relay";} else { event.stroke = "Breaststroke";} break;
                                case "2": if(gen.equals("R")){event.stroke = "Relay";} else { event.stroke = "Backstroke";} break;
                                case "3": if(gen.equals("R")){event.stroke = "Relay";} else { event.stroke = "Butterfly";} break;
                                case "4": if(gen.equals("R")){event.stroke = "Relay";} else { event.stroke = "Medley";} break;
                                default: event.stroke = "Relay";
                            }
                            switch (gen) {
                                case "G": event.gender = "female"; break;
                                case "B": event.gender = "male"; break;
                                default: event.gender = "mix";
                            }
                            event.eventname = event.stroke + ", " + event.distance + " (" + event.gender + ")";
                            event.realeventorder = linecounter;
                            event.heatquantity = Integer.parseInt(data[2]);

                            for (int j = 0; j < Integer.parseInt(data[2]); j++) {
                                Heat heat = new Heat(event.stroke + ", " + event.distance, j+1, this);
                                for (int k = 0; k < this.lanes; k++) {
                                    heat.heatparticipants.add(k, null);
                                }
                                event.heatlist.add(j, heat);
                            }
                            this.eventmap.put(Integer.parseInt(data[0]), event);
                            }

                        linecounter++;
                    }
                } catch (Exception ex){ System.out.println(ex.getMessage()); }
            }
        }
        //����������� �������
        for (int i = 0; i <3 ; i++) {
            if (headers.get(i).length == 6){
                try  {
                    FileInputStream swim = new FileInputStream(files[i]);
                    BufferedReader swimreader = new BufferedReader(new InputStreamReader(swim, "UTF-8"));
                    Scanner swimscan = new Scanner(swimreader);
                    int linecounter = 0;
                    while (swimscan.hasNextLine()){

                            String[] data = swimscan.nextLine().split(";");

                            if(linecounter>0) {
                            if(this.eventmap.get(Integer.parseInt(data[0])).stroke.equals("Relay") && data[4].equals("0") ){
                                this.eventmap.get(Integer.parseInt(data[0])).heatlist.get(Integer.parseInt(data[2])).heatparticipants.set(Integer.parseInt(data[3]),relayteamlist.get(data[5]));
                            } else if ((!(this.eventmap.get(Integer.parseInt(data[0])).stroke.equals("Relay"))) && data[4].equals("0") ){
                                this.eventmap.get(Integer.parseInt(data[0])).heatlist.get(Integer.parseInt(data[2])).heatparticipants.set(Integer.parseInt(data[3]),swimmerdb.get(data[5]));
                            }

                        }
                        linecounter++;
                    }
                } catch (Exception ex){ ex.getMessage(); }
            }
        }

        JSONObject destinationjson = new JSONObject();

        destinationjson.put("name", this.competitionname);
        destinationjson.put("place", this.poolname);
        destinationjson.put("date", this.date);
        destinationjson.put("lanes", this.lanes);
        destinationjson.put("poollength", this.poollength);
        destinationjson.put("picture", this.pictureadress);
        destinationjson.put("start",df.format(this.warmupstarttime));

        String finale = destinationjson.toJSONString();

        try(FileWriter file = new FileWriter("file1.txt")){
            file.write(finale);
            System.out.println("Object: "+ destinationjson);
        }

    }



    void printdestination() {
        System.out.println(Colors.BLACK);
        System.out.println("Welcome to " + this.competitionname + "!\n" + Colors.RESET);
        System.out.println(Colors.YELLOW_BRIGHT + "Destination: " + Colors.RESET + this.poolname);
        System.out.println(Colors.YELLOW_BRIGHT + "Date: " + Colors.RESET + this.date);
        System.out.println(Colors.YELLOW_BRIGHT + "Pool length: " + Colors.RESET + this.poollength + Colors.YELLOW_BRIGHT +
                " m, pool has " + Colors.RESET + this.lanes + Colors.YELLOW_BRIGHT + " lanes.");
        System.out.print(Colors.RESET);
    }


    void formrealeventlist(Drive servicedrive) throws IOException {
        TreeMap<Integer, int[]> lanesets = new TreeMap<>();

        lanesets.put(1, new int[]{0});
        lanesets.put(2, new int[]{1,0});
        lanesets.put(3, new int[]{0,2,1});
        lanesets.put(4, new int[]{3,0,2,1});
        lanesets.put(5, new int[]{0,4,1,3,2});
        lanesets.put(6, new int[]{5,0,4,1,3,2});
        lanesets.put(7, new int[]{0,6,1,5,2,4,3});
        lanesets.put(8, new int[]{7,0,6,1,5,2,4,3});
        lanesets.put(9, new int[]{0,8,1,7,2,6,3,5,4});
        lanesets.put(10, new int[]{9,0,8,1,7,2,6,3,5,4});

        for (Event event : fulleventlist) {
            if (event.eventparticipants.size()>1){
               this.realeventlist.add(event);
            }
        }
        this.realeventlist.sort(new EventComparator());
        //������� ��������
        int eventordercode = 0;
        for (Event event : realeventlist) {
                this.eventmap.put(eventordercode,event);
                eventordercode++;
        }


        int neweventorder = 1;
        for (Integer key : this.eventmap.keySet()){

            eventmap.get(key).eventname = eventmap.get(key).stroke + ", " + eventmap.get(key).distance + " (" + eventmap.get(key).gender + " " + eventmap.get(key).age + ")";

//            Voice Llamavoice = new Voice("dfki-poppy-hsmm");
            Voice Llamavoice = new Voice("cmu-rms-hsmm");
            if(!eventmap.get(key).age.contains("-")) {
                Llamavoice.say("Next event is " + eventmap.get(key).distance + " meters " + eventmap.get(key).stroke + "," + eventmap.get(key).gender + "s age " + eventmap.get(key).age, eventmap.get(key).eventname);
            } else if(eventmap.get(key).age.endsWith("AA")){
                String newage = eventmap.get(key).age.substring(0,eventmap.get(key).age.length()-2)+"21";
                Llamavoice.say("Next event is " + eventmap.get(key).distance + " meters " + eventmap.get(key).stroke + "," + eventmap.get(key).gender + "s from " + newage, eventmap.get(key).eventname);
            } else {
                Llamavoice.say("Next event is " + eventmap.get(key).distance + " meters " + eventmap.get(key).stroke + "," + eventmap.get(key).gender + "s from " + eventmap.get(key).age, eventmap.get(key).eventname);
            }
            //�������� ���������������
            String folderId = "1GpWNM08mFZzj8_NZbi_wOv6hT4zOkpNE";
            File fileMetadata = new File();
            fileMetadata.setName(eventmap.get(key).eventname+".wav");
            fileMetadata.setParents(Collections.singletonList(folderId));
            java.io.File filePath = new java.io.File(eventmap.get(key).eventname+".wav");
            FileContent mediaContent = new FileContent(eventmap.get(key).eventname+"/wav", filePath);
            File file = servicedrive.files().create(fileMetadata, mediaContent).setFields("id, parents").execute();
            
            eventmap.get(key).audioadress= file.getId();
            FileList data = servicedrive.files().list().execute();

            //������� ������-�����������
                if (neweventorder % this.awardfrequency == 0 && neweventorder!=realeventlist.size()) {
                    Event award = new Event();
                    award.eventname = "Intermediate awarding ceremony";
                    this.awardlist.add(award);
                }

            eventmap.get(key).realeventorder = neweventorder;
                neweventorder++;

                //��������� ������� � ������� ����
                SwimmerComparator comp = new SwimmerComparator();
                comp.strokedistance = eventmap.get(key).stroke+eventmap.get(key).distance;
            eventmap.get(key).eventparticipants.sort(comp);

                //���������� �����
                if (eventmap.get(key).eventparticipants.size()%this.lanes==0) {
                    eventmap.get(key).heatquantity = eventmap.get(key).eventparticipants.size() / this.lanes;
                } else {
                    eventmap.get(key).heatquantity = (int)(eventmap.get(key).eventparticipants.size() / this.lanes)+1;
                }

                //����� ������� �������� �� �����
         //������ �������
                if (eventmap.get(key).eventparticipants.size()%this.lanes==0){
                    for (int i = 0; i <eventmap.get(key).heatquantity ; i++) {
                        Heat heat = new Heat(eventmap.get(key).stroke+", "+eventmap.get(key).distance, i+1, this);
                        //�������� ������
                        for (int j = 0; j <this.lanes ; j++) {
                         heat.heatparticipants.add(j,null);
                        }
                        for (int j = 0; j <this.lanes ; j++) {
                        heat.heatparticipants.set(lanesets.get(this.lanes)[j], eventmap.get(key).eventparticipants.get(j+this.lanes*i));
                        }
                        eventmap.get(key).heatlist.add(heat);
                    }
                } else if (eventmap.get(key).eventparticipants.size()%this.lanes>1) {
         //������ ���������
                    //������ ���
                    Heat heat = new Heat(eventmap.get(key).stroke + ", " + eventmap.get(key).distance, 1, this);
                    for (int j = 0; j < this.lanes; j++) {
                        heat.heatparticipants.add(j, null);
                    }

                    if ((this.lanes - eventmap.get(key).eventparticipants.size()%this.lanes) % 2 == 0) {

                        for (int j = 0; j < eventmap.get(key).eventparticipants.size() % this.lanes; j++) {
                            heat.heatparticipants.set(lanesets.get(eventmap.get(key).eventparticipants.size() % this.lanes)[j] + (int) ((this.lanes - eventmap.get(key).eventparticipants.size() % this.lanes) / 2), eventmap.get(key).eventparticipants.get(j));
                        }
                    } else {
                        for (int j = 0; j < eventmap.get(key).eventparticipants.size() % this.lanes; j++) {
                            heat.heatparticipants.set(lanesets.get(eventmap.get(key).eventparticipants.size() % this.lanes)[j] + (int) ((this.lanes - eventmap.get(key).eventparticipants.size() % this.lanes) / 2) + 1, eventmap.get(key).eventparticipants.get(j));
                        }
                    }
                    eventmap.get(key).heatlist.add(heat);

                    //���� ����� �������

                    if (eventmap.get(key).heatquantity>1) {
                        for (int i = 1; i < eventmap.get(key).heatquantity; i++) {
                            Heat heat1 = new Heat(eventmap.get(key).stroke + ", " + eventmap.get(key).distance, i + 1, this);
                            //�������� ������
                            for (int j = 0; j < this.lanes; j++) {
                                heat1.heatparticipants.add(j, null);
                            }
                            for (int j = 0; j < this.lanes; j++) {
                                heat1.heatparticipants.set(lanesets.get(this.lanes)[j], eventmap.get(key).eventparticipants.get(j + this.lanes * (i - 1) + eventmap.get(key).eventparticipants.size() % this.lanes));
                            }
                            eventmap.get(key).heatlist.add(heat1);
                        }
                    }

                    } else {
            //���� ���� ������ ��������

                    //������ ��� �� 2
                    Heat heat = new Heat(eventmap.get(key).stroke + ", " + eventmap.get(key).distance, 1, this);
                    for (int j = 0; j < this.lanes; j++) {
                        heat.heatparticipants.add(j, null);
                    }

                    if (this.lanes% 2 == 0) {

                        for (int j = 0; j < 2; j++) {
                            heat.heatparticipants.set(lanesets.get(2)[j] + (int) ((this.lanes - 2) / 2), eventmap.get(key).eventparticipants.get(j));
                        }
                    } else {
                        for (int j = 0; j < 2; j++) {
                            heat.heatparticipants.set(lanesets.get(2)[j] + (int) ((this.lanes - 2) / 2) + 1, eventmap.get(key).eventparticipants.get(j));
                        }
                    }
                    eventmap.get(key).heatlist.add(heat);

                    //������ ��� �� lanes-1
                    Heat heat1 = new Heat(eventmap.get(key).stroke + ", " + eventmap.get(key).distance, 2, this);
                    for (int j = 0; j < this.lanes; j++) {
                        heat1.heatparticipants.add(j, null);
                    }
                        for (int j = 0; j < this.lanes-1; j++) {
                            heat1.heatparticipants.set(lanesets.get(this.lanes-1)[j]+ 1, eventmap.get(key).eventparticipants.get(j+2));
                        }
                    eventmap.get(key).heatlist.add(heat1);

                    //���� ����� �������

                    if (eventmap.get(key).heatquantity>2) {
                        for (int i = 2; i < eventmap.get(key).heatquantity; i++) {
                            Heat heat2 = new Heat(eventmap.get(key).stroke + ", " + eventmap.get(key).distance, i + 1, this);
                            //�������� ������
                            for (int j = 0; j < this.lanes; j++) {
                                heat2.heatparticipants.add(j, null);
                            }
                            for (int j = 0; j < this.lanes; j++) {
                                heat2.heatparticipants.set(lanesets.get(this.lanes)[j], eventmap.get(key).eventparticipants.get(j + this.lanes+1));
                            }
                            eventmap.get(key).heatlist.add(heat2);
                        }
                    }

                }
    //��������� �����
        }

        //��� ��� ������ ����� ������ �� �����
//        for (Event event: realeventlist
//             ) {
//            System.out.println("          "+event.eventname);
//            for (Heat heat: event.heatlist
//                 ) {
//                System.out.println("   "+heat.heatname);
//                for (Swimmer guy: heat.heatparticipants
//                     ) {
//                    System.out.println(guy);
//                }
//
//
//            }
//        }
    }

    void printtimetable() {
        System.out.println();
        System.out.println(Colors.BLACK + "Event list:" + Colors.RESET);
        System.out.println();

        for (Integer key : this.eventmap.keySet()) {
            eventmap.get(key).printheats();
        }
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        System.out.println("Competition closing time: "+df.format(this.awardlist.get(this.awardlist.size()-1).eventstarttime));
    }

//���������� �������
    void settimealignment() {
        Date currenttime = compstarttime;

        int eventnum = 0;
        int awardnum = 0;
        for (Integer key : this.eventmap.keySet()) {
            eventmap.get(key).eventstarttime = currenttime;

                for (Heat eventheat : eventmap.get(key).heatlist) {
                    eventheat.heatstarttime = currenttime;
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(currenttime);
                    cal.add(Calendar.MINUTE, this.heattimes.get(eventmap.get(key).distance));
                    currenttime = cal.getTime();

                    if(eventheat.heatnum < eventmap.get(key).heatquantity){
                        Calendar cal1 = Calendar.getInstance();
                        cal1.setTime(currenttime);
                        cal1.add(Calendar.MINUTE, this.heatinterval);
                        currenttime = cal1.getTime();
                    }
                }
                eventnum++;

                if (eventnum > 0 && eventnum != this.realeventlist.size() && eventnum % awardfrequency == 0) {
                    this.awardlist.get(awardnum).eventstarttime = currenttime;
                    awardnum++;

                    Calendar cal = Calendar.getInstance();
                    cal.setTime(currenttime);
                    cal.add(Calendar.MINUTE, this.awardlength);
                    currenttime = cal.getTime();
                }

            if (eventmap.get(key).realeventorder!=eventmap.size()) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(currenttime);
                cal.add(Calendar.MINUTE, this.eventinterval);
                currenttime = cal.getTime();
            }
        }

        Event finalceremony = new Event();
        finalceremony.eventname = "Final awarding ceremony";
        finalceremony.eventstarttime = currenttime;
        Calendar cal = Calendar.getInstance();
        cal.setTime(currenttime);
        cal.add(Calendar.MINUTE, 15);
        currenttime = cal.getTime();

        Event compclosing = new Event();
        compclosing.eventname = "Competition closing";
        compclosing.eventstarttime = currenttime;
        this.awardlist.add(finalceremony);
        this.awardlist.add(compclosing);
    }

    void createreport(Drive servicedrive) throws IOException, DocumentException {

//        System.out.println("Enter command: ");
//        //���� �������
//        String command = "";
//        Scanner scanner = new Scanner(System.in);
//        command = scanner.nextLine();
//
//        //��� ��� ������� ����� ��� �������� � ������
//
//        //�������� ���������� �����
//
//        if (command.contains("go")) {

                //�������� ������� ������

            Document report_pdf = new Document();
            report_pdf.setMargins(0, 0, 30, 10);
            PdfWriter.getInstance(report_pdf, new FileOutputStream("timetable.pdf"));
            FileOutputStream fo = new FileOutputStream("timetable.pdf" );

            report_pdf.open();


            PdfPTable reporttable = new PdfPTable(8);
            reporttable.setWidthPercentage(90);
            reporttable.setWidths(new float[]{15f,6f,14f,14f,6f,15f,15f,15f});
            BaseColor blueback = new BaseColor(222, 234, 246);
            BaseColor salad = new BaseColor(242, 255, 175);
            BaseColor golden = new BaseColor(255, 229, 152);
            BaseColor orange = new BaseColor(255, 153, 0);


            Font blueunderlined = new Font(Font.FontFamily.HELVETICA, 10, Font.UNDERLINE, BaseColor.BLUE);
            blueunderlined.setColor(0, 0, 128);
            Font smalldivider = new Font(Font.FontFamily.HELVETICA, 3, Font.BOLD, BaseColor.WHITE);
            Font boldviolfont = new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD, BaseColor.MAGENTA);
            Font boldblackfont = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD, BaseColor.BLACK);
            Font boldbigblackfont = new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD, BaseColor.BLACK);
            Font basicgreyfont = new Font(Font.FontFamily.HELVETICA, 14, Font.NORMAL, BaseColor.DARK_GRAY);
            Font redfont = new Font(Font.FontFamily.HELVETICA, 14, Font.NORMAL, BaseColor.RED);
            Font italicboldgreenfont = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLDITALIC, BaseColor.GREEN);
            Font boldbluefont = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD, BaseColor.BLUE);
            FontFactory.register("Rubik-Regular.ttf", "hebrew");
            Font blackhebrew = FontFactory.getFont("hebrew", "Identity-H", true);

            Font bluebackfont = new Font(Font.FontFamily.HELVETICA, 14, Font.NORMAL, blueback);
            italicboldgreenfont.setColor(0, 128, 0);
            boldviolfont.setColor(128, 0, 128);


            PdfPCell cellheader = new PdfPCell(new Phrase(this.competitionname, boldviolfont));
            cellheader.setHorizontalAlignment(1);
            cellheader.setVerticalAlignment(2);
            cellheader.setColspan(reporttable.getNumberOfColumns());
            cellheader.setBorder(0);
            reporttable.addCell(cellheader);

            PdfPCell cellheader1 = new PdfPCell(new Phrase("       ", boldviolfont));
            cellheader1.setColspan(reporttable.getNumberOfColumns());
            cellheader1.setBorder(0);
            reporttable.addCell(cellheader1);

            PdfPCell destinheader = new PdfPCell(new Phrase("Destination: ", boldblackfont));
            destinheader.setBackgroundColor(blueback);
            destinheader.setRowspan(2);
            destinheader.setColspan(2);
            destinheader.setFixedHeight(30);
            destinheader.setBorder(0);
            reporttable.addCell(destinheader);

            PdfPCell destin = new PdfPCell(new Phrase(this.poolname, italicboldgreenfont));
            destin.setRowspan(2);
            destin.setColspan((reporttable.getNumberOfColumns() - 2) / 2+1);
            destin.setBorder(0);
            reporttable.addCell(destin);

            Image ssw;
            if(this.pictureadress == ""){
                ssw = Image.getInstance("https://pp.userapi.com/c845016/v845016467/18a573/jsqiy6MfBoI.jpg");
            } else {
                ssw = Image.getInstance(this.pictureadress);
            }
                ssw.scaleAbsoluteHeight(60);
                ssw.scaleAbsoluteWidth(150);
            PdfPCell image = new PdfPCell(ssw);
            image.setRowspan(4);
            image.setColspan((reporttable.getNumberOfColumns() - 2) / 2-1);
            image.setBorder(0);
            reporttable.addCell(image);

            PdfPCell dateheader = new PdfPCell(new Phrase("Date: ", boldblackfont));
            dateheader.setBackgroundColor(blueback);
            dateheader.setRowspan(2);
            dateheader.setColspan(2);
            dateheader.setFixedHeight(30);
            dateheader.setBorder(0);
            reporttable.addCell(dateheader);

            PdfPCell date = new PdfPCell(new Phrase(this.date, italicboldgreenfont));
            date.setRowspan(2);
            date.setColspan((reporttable.getNumberOfColumns() - 2) / 2+1);
            date.setBorder(0);
            reporttable.addCell(date);

            PdfPCell poolheader = new PdfPCell(new Phrase("Pool dimensions: ", boldblackfont));
            poolheader.setBackgroundColor(blueback);
            poolheader.setRowspan(2);
            poolheader.setColspan(2);
            poolheader.setFixedHeight(40);
            poolheader.setBorder(0);
            reporttable.addCell(poolheader);

            PdfPCell poollengthheader = new PdfPCell(new Phrase("Lane length, m:", boldbluefont));
            poollengthheader.setColspan((reporttable.getNumberOfColumns() - 2) / 2 - 1);
            poollengthheader.setBorder(0);
            reporttable.addCell(poollengthheader);

            PdfPCell poollength = new PdfPCell(new Paragraph(String.valueOf(this.poollength), boldbluefont));
            poollength.setColspan((reporttable.getNumberOfColumns() - 2) / 2 + 1);
            poollength.setBorder(0);
            reporttable.addCell(poollength);

            PdfPCell poollanesheader = new PdfPCell(new Phrase("Lanes assigned:", boldbluefont));
            poollanesheader.setColspan((reporttable.getNumberOfColumns() - 2) / 2 - 1);
            poollanesheader.setBorder(0);
            reporttable.addCell(poollanesheader);

            PdfPCell poollanes = new PdfPCell(new Paragraph(String.valueOf(this.lanes), boldbluefont));
            poollanes.setColspan((reporttable.getNumberOfColumns() - 2) / 2 + 1);
            poollanes.setBorder(0);
            reporttable.addCell(poollanes);

            PdfPCell timeheader0 = new PdfPCell(new Phrase("       ", boldbigblackfont));
            timeheader0.setColspan(reporttable.getNumberOfColumns());
            timeheader0.setBorder(0);
            reporttable.addCell(timeheader0);

            PdfPCell timeheader = new PdfPCell(new Phrase("Competition timetable", boldbigblackfont));
            timeheader.setHorizontalAlignment(1);
            timeheader.setVerticalAlignment(2);
            timeheader.setColspan(reporttable.getNumberOfColumns());
            timeheader.setBorder(0);
            reporttable.addCell(timeheader);

            PdfPCell timeheader1 = new PdfPCell(new Phrase("       ", boldbigblackfont));
            timeheader1.setColspan(reporttable.getNumberOfColumns());
            timeheader1.setBorder(0);
            reporttable.addCell(timeheader1);


            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");

            //�������� � ��������
            if(this.warmupstarttime!=this.compstarttime) {
                PdfPCell warmup = new PdfPCell(new Phrase("Warmup", boldblackfont));
                warmup.setHorizontalAlignment(1);
                warmup.setVerticalAlignment(Element.ALIGN_MIDDLE);
                warmup.setColspan(reporttable.getNumberOfColumns() - 1);
                warmup.setBackgroundColor(orange);
                warmup.setFixedHeight(25);
                warmup.setBorder(0);
                reporttable.addCell(warmup);

                PdfPCell warmuptime = new PdfPCell(new Phrase(df.format(warmupstarttime), boldblackfont));
                warmuptime.setHorizontalAlignment(1);
                warmuptime.setVerticalAlignment(Element.ALIGN_MIDDLE);
                warmuptime.setBackgroundColor(orange);
                warmuptime.setFixedHeight(25);
                warmuptime.setBorder(0);
                reporttable.addCell(warmuptime);

                PdfPCell warmupblank = new PdfPCell(new Phrase("       ", boldviolfont));
                warmupblank.setColspan(reporttable.getNumberOfColumns());
                warmupblank.setFixedHeight(5);
                warmupblank.setBorder(0);
                reporttable.addCell(warmupblank);
            }
            if(this.openingtime!=this.compstarttime) {
                PdfPCell opening = new PdfPCell(new Phrase("Competition opening ceremony", boldblackfont));
                opening.setHorizontalAlignment(1);
                opening.setVerticalAlignment(Element.ALIGN_MIDDLE);
                opening.setColspan(reporttable.getNumberOfColumns() - 1);
                opening.setBackgroundColor(orange);
                opening.setFixedHeight(25);
                opening.setBorder(0);
                reporttable.addCell(opening);

                PdfPCell opentime = new PdfPCell(new Phrase(df.format(openingtime), boldblackfont));
                opentime.setHorizontalAlignment(1);
                opentime.setVerticalAlignment(Element.ALIGN_MIDDLE);
                opentime.setBackgroundColor(orange);
                opentime.setFixedHeight(25);
                opentime.setBorder(0);
                reporttable.addCell(opentime);

                PdfPCell openingblank = new PdfPCell(new Phrase("       ", boldviolfont));
                openingblank.setColspan(reporttable.getNumberOfColumns());
                openingblank.setFixedHeight(5);
                openingblank.setBorder(0);
                reporttable.addCell(openingblank);
            }

            int eventnum = 1;
            int awardnum = 0;

            for (Integer key: this.eventmap.keySet()){

                //������ �� ������
                PdfPCell eventheader = new PdfPCell(new Phrase(eventmap.get(key).realeventorder + ". " + eventmap.get(key).eventname, boldblackfont));
                eventheader.setHorizontalAlignment(0);
                eventheader.setColspan(reporttable.getNumberOfColumns() - 1);
                eventheader.setBackgroundColor(salad);
                eventheader.setFixedHeight(25);
                eventheader.setBorder(0);
                reporttable.addCell(eventheader);

                PdfPCell eventtime = new PdfPCell(new Phrase(df.format(eventmap.get(key).eventstarttime), boldblackfont));
                eventtime.setHorizontalAlignment(1);
                eventtime.setBackgroundColor(salad);
                eventtime.setFixedHeight(25);
                eventtime.setBorder(0);
                reporttable.addCell(eventtime);

//                if (command.contains("full")) {

                    //���������
                    PdfPCell heatheader = new PdfPCell(new Phrase(" "));
                    heatheader.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    heatheader.setHorizontalAlignment(1);
                    heatheader.setBorder(0);
                    reporttable.addCell(heatheader);

                    PdfPCell heatlaner = new PdfPCell(new Phrase("Lane"));
                    heatlaner.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    heatlaner.setHorizontalAlignment(1);
                    heatlaner.setBorder(0);
                    reporttable.addCell(heatlaner);

                    if(eventmap.get(key).eventname.contains("Relay")){
                        PdfPCell heatnamer = new PdfPCell(new Phrase("Relay team name"));
                        heatnamer.setBackgroundColor(BaseColor.LIGHT_GRAY);
                        heatnamer.setColspan(2);
                        heatnamer.setHorizontalAlignment(1);
                        heatnamer.setBorder(0);
                        reporttable.addCell(heatnamer);
                    } else {
                        PdfPCell heatnamer = new PdfPCell(new Phrase("Swimmer name"));
                        heatnamer.setBackgroundColor(BaseColor.LIGHT_GRAY);
                        heatnamer.setColspan(2);
                        heatnamer.setHorizontalAlignment(1);
                        heatnamer.setBorder(0);
                        reporttable.addCell(heatnamer);
                    }
                    PdfPCell heatager = new PdfPCell(new Phrase("Age"));
                    heatager.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    heatager.setHorizontalAlignment(1);
                    heatager.setBorder(0);
                    reporttable.addCell(heatager);

                    PdfPCell heatteamer = new PdfPCell(new Phrase("Team"));
                    heatteamer.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    heatteamer.setHorizontalAlignment(1);
                    heatteamer.setBorder(0);
                    reporttable.addCell(heatteamer);

                    PdfPCell heatentrier = new PdfPCell(new Phrase("Entry time"));
                    heatentrier.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    heatentrier.setHorizontalAlignment(1);
                    heatentrier.setBorder(0);
                    reporttable.addCell(heatentrier);

                    PdfPCell heatheaderfin = new PdfPCell(new Phrase("Heat start"));
                    heatheaderfin.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    heatheaderfin.setHorizontalAlignment(1);
                    heatheaderfin.setBorder(0);
                    reporttable.addCell(heatheaderfin);

                    PdfPCell eventblank1 = new PdfPCell(new Phrase("       ", boldviolfont));
                    eventblank1.setColspan(reporttable.getNumberOfColumns());
                    eventblank1.setFixedHeight(5);
                    eventblank1.setBorder(0);
                    reporttable.addCell(eventblank1);

                    int heatnum = 1;
                    for (Heat eventheat : eventmap.get(key).heatlist) {
                        //������ ������ �� ����

                        PdfPCell heatnumber = new PdfPCell(new Phrase("Heat " + heatnum, boldblackfont));
                        heatnumber.setRowspan(this.lanes);
                        heatnumber.setBackgroundColor(blueback);
                        heatnumber.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        heatnumber.setBorder(0);
                        reporttable.addCell(heatnumber);

                        //������ ������
                        for (int i = 0; i < this.lanes; i++) {

                            PdfPCell lanename = new PdfPCell(new Phrase("" + (i + 1), blueunderlined));
                            lanename.setHorizontalAlignment(1);
                            lanename.setBorder(0);
                            reporttable.addCell(lanename);

                            if (eventheat.heatparticipants.get(i) != null) {

                                PdfPCell name = new PdfPCell(new Phrase(process(eventheat.heatparticipants.get(i).familyname) + " " + process(eventheat.heatparticipants.get(i).name), blackhebrew));

                                name.setColspan(2);
                                name.setHorizontalAlignment(1);
                                name.setBorder(0);
                                reporttable.addCell(name);
                            } else {
                                PdfPCell name = new PdfPCell(new Phrase("-"));
                                name.setColspan(2);
                                name.setHorizontalAlignment(1);
                                name.setBorder(0);
                                reporttable.addCell(name);
                            }

                            if (eventheat.heatparticipants.get(i) != null) {
                                PdfPCell age = new PdfPCell(new Phrase("" + eventheat.heatparticipants.get(i).birthyear));
                                age.setHorizontalAlignment(1);
                                age.setBorder(0);
                                reporttable.addCell(age);
                            } else {
                                PdfPCell age = new PdfPCell(new Phrase("-"));
                                age.setHorizontalAlignment(1);
                                age.setBorder(0);
                                reporttable.addCell(age);
                            }

                            if (eventheat.heatparticipants.get(i) != null) {
                                PdfPCell team = new PdfPCell(new Phrase(process(eventheat.heatparticipants.get(i).team)));
                                team.setHorizontalAlignment(1);
                                team.setBorder(0);
                                reporttable.addCell(team);
                            } else {
                                PdfPCell team = new PdfPCell(new Phrase("-"));
                                team.setHorizontalAlignment(1);
                                team.setBorder(0);
                                reporttable.addCell(team);
                            }

                            if (eventheat.heatparticipants.get(i) != null) {
                                if (eventheat.heatparticipants.get(i).recordlist.size() > 0) {
                                    PdfPCell entry = new PdfPCell(new Phrase(eventheat.heatparticipants.get(i).recordlist.get(eventmap.get(key).stroke + eventmap.get(key).distance).recordstring, redfont));
                                    entry.setHorizontalAlignment(1);
                                    entry.setBorder(0);
                                    reporttable.addCell(entry);
                                } else {
                                    PdfPCell entry = new PdfPCell(new Phrase(" - ", redfont));
                                    entry.setHorizontalAlignment(1);
                                    entry.setBorder(0);
                                    reporttable.addCell(entry);
                                }

                            } else {
                                PdfPCell entry = new PdfPCell(new Phrase("-"));
                                entry.setHorizontalAlignment(1);
                                entry.setBorder(0);
                                reporttable.addCell(entry);
                            }

                            if (i == 0) {

                                PdfPCell heattimer = new PdfPCell(new Phrase(df.format(eventheat.heatstarttime), boldblackfont));
                                heattimer.setRowspan(this.lanes);
                                heattimer.setBackgroundColor(blueback);
                                heattimer.setVerticalAlignment(Element.ALIGN_MIDDLE);
                                heattimer.setHorizontalAlignment(1);
                                heattimer.setBorder(0);
                                reporttable.addCell(heattimer);
                            }

                        }

                        PdfPCell heatblank1 = new PdfPCell(new Phrase("       ", boldviolfont));
                        heatblank1.setColspan(reporttable.getNumberOfColumns());
                        heatblank1.setFixedHeight(5);
                        heatblank1.setBorder(0);
                        reporttable.addCell(heatblank1);

                        heatnum++;
                    }
//                }
                    if (eventnum % awardfrequency == 0) {

                        PdfPCell awardheader = new PdfPCell(new Phrase(awardlist.get(awardnum).eventname, boldblackfont));
                        awardheader.setHorizontalAlignment(1);
                        awardheader.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        awardheader.setColspan(reporttable.getNumberOfColumns() - 1);
                        awardheader.setBackgroundColor(golden);
                        awardheader.setFixedHeight(40);
                        awardheader.setBorder(0);
                        reporttable.addCell(awardheader);

                        PdfPCell awardtime = new PdfPCell(new Phrase(df.format(awardlist.get(awardnum).eventstarttime), boldblackfont));
                        awardtime.setHorizontalAlignment(1);
                        awardtime.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        awardtime.setBackgroundColor(golden);
                        awardtime.setFixedHeight(40);
                        awardtime.setBorder(0);
                        reporttable.addCell(awardtime);

                        PdfPCell awardblank = new PdfPCell(new Phrase("       ", boldviolfont));
                        awardblank.setColspan(reporttable.getNumberOfColumns());
                        awardblank.setFixedHeight(5);
                        awardblank.setBorder(0);
                        reporttable.addCell(awardblank);

                        awardnum++;

                    }

                    eventnum++;

            }
//��������� �����������
            for (int i = awardnum; i < awardlist.size(); i++) {

                PdfPCell awardheader = new PdfPCell(new Phrase(awardlist.get(awardnum).eventname, boldblackfont));
                awardheader.setHorizontalAlignment(1);
                awardheader.setVerticalAlignment(Element.ALIGN_MIDDLE);
                awardheader.setColspan(reporttable.getNumberOfColumns() - 1);
                if(i == awardlist.size()-1) {
                    awardheader.setBackgroundColor(orange);
                    awardheader.setFixedHeight(25);
                }else {
                    awardheader.setBackgroundColor(golden);
                    awardheader.setFixedHeight(40);
                }
                awardheader.setBorder(0);
                reporttable.addCell(awardheader);

                PdfPCell awardtime = new PdfPCell(new Phrase(df.format(awardlist.get(awardnum).eventstarttime), boldblackfont));
                awardtime.setHorizontalAlignment(1);
                awardtime.setVerticalAlignment(Element.ALIGN_MIDDLE);
                if(i == awardlist.size()-1) {
                    awardtime.setBackgroundColor(orange);
                    awardtime.setFixedHeight(25);
                }else {
                    awardtime.setBackgroundColor(golden);
                    awardtime.setFixedHeight(40);
                }
                awardtime.setBorder(0);
                reporttable.addCell(awardtime);

                PdfPCell awardblank = new PdfPCell(new Phrase("       ", boldviolfont));
                awardblank.setColspan(reporttable.getNumberOfColumns());
                awardblank.setFixedHeight(5);
                awardblank.setBorder(0);
                reporttable.addCell(awardblank);

                awardnum++;

            }

// next row

            report_pdf.add(reporttable);
            report_pdf.close();

//        }

            String folderId = "1GpWNM08mFZzj8_NZbi_wOv6hT4zOkpNE";
            File fileMetadata = new File();
            fileMetadata.setName("timetable.pdf");
            fileMetadata.setParents(Collections.singletonList(folderId));
            java.io.File filePath = new java.io.File("timetable.pdf");
            FileContent mediaContent = new FileContent("timetable/pdf", filePath);
            File file = servicedrive.files().create(fileMetadata, mediaContent).setFields("id, parents").execute();
            
            this.timetablefulllink = file.getId();
            FileList data = servicedrive.files().list().execute();
            
    }

    public static String process(String s) {
        return BidiLine.processLTR(s, PdfWriter.RUN_DIRECTION_RTL,0);
    }



//    public void createPdf(String file) throws IOException, DocumentException {
//        // step 1
//        Document document = new Document();
//        // step 2
//        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));
//        // step 3
//        document.open();
//        // step 4
//        // Styles
//        CSSResolver cssResolver = new StyleAttrCSSResolver();
//        XMLWorkerFontProvider fontProvider = new XMLWorkerFontProvider(XMLWorkerFontProvider.DONTLOOKFORFONTS);
//        fontProvider.register("resources/fonts/NotoSansHebrew-Regular.ttf");
//        CssAppliers cssAppliers = new CssAppliersImpl(fontProvider);
//        HtmlPipelineContext htmlContext = new HtmlPipelineContext(cssAppliers);
//        htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
//
//        // Pipelines
//        PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer);
//        HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
//        CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
//
//        // XML Worker
//        XMLWorker worker = new XMLWorker(css, true);
//        XMLParser p = new XMLParser(worker);
//        p.parse(new FileInputStream(HTML), Charset.forName("UTF-8"));;
//        // step 5
//        document.close();
//    }


}

class EventComparator implements Comparator<Event> {

    public int compare(Event a, Event b) {
        if (a.eventorder < b.eventorder) {
            return -1;
        } else if (a.eventorder > b.eventorder) {
            return 1;
        } else {
            if (a.distance < b.distance) {
                return -1;
            } else if (a.distance < b.distance) {
                return 1;
            } else if (a.gender.equals("female")) {
                return -1;
            } else {
                return 1;
            }
        }

    }
}

class SwimmerComparator implements Comparator<Swimmer> {
    String strokedistance;

    public int compare(Swimmer a, Swimmer b) {
        if (a.recordlist.get(strokedistance).recordcs < b.recordlist.get(strokedistance).recordcs) {
            return 1;
        } else if (a.recordlist.get(strokedistance).recordcs > b.recordlist.get(strokedistance).recordcs) {
            return -1;
        } else {
            if ((a.name+a.familyname).compareToIgnoreCase(b.name+b.familyname) > 0) {
                return -1;
            } else if ((a.name+a.familyname).compareToIgnoreCase(b.name+b.familyname) < 0) {
                return 1;
            } else if (a.age>b.age) {
                return 1;
            } else {
                return -1;
            }
        }

    }
}

class CurrentResultComparator implements Comparator<Swimmer> {
    
    public int compare(Swimmer a, Swimmer b) {
        if (a.currentresult > b.currentresult) {
            return 1;
        } else if (a.currentresult < b.currentresult) {
            return -1;
        } else {
            if ((a.name+a.familyname).compareToIgnoreCase(b.name+b.familyname) > 0) {
                return -1;
            } else if ((a.name+a.familyname).compareToIgnoreCase(b.name+b.familyname) < 0) {
                return 1;
            } else if (a.age>b.age) {
                return 1;
            } else {
                return -1;
            }
        }

    }
}