import javafx.util.converter.LocalTimeStringConverter;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;

public class Event {
    Date eventstarttime;
    Date eventfintime;

    String stroke="";
    int distance = 0;
    String gender = "";
    String age = "";
    String eventname;
    String audioadress;

    int eventorder;
    int realeventorder;
    int heatquantity;

    ArrayList<Swimmer> eventparticipants;
    ArrayList<Heat> heatlist;

    public Event(){
        this.eventparticipants = new ArrayList<Swimmer>();
        this.heatlist = new ArrayList<Heat>();
        this.heatquantity = 0;
    }

    void printheats() {
        if (heatquantity > 0){
            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
            System.out.println(Colors.PURPLE_BOLD_BRIGHT + this.realeventorder+". "+ this.eventname + Colors.RESET + df.format(this.eventstarttime) +"\n");
        for (Heat go : heatlist) {
            go.printheatparticipants();
            System.out.println();
            }
        }
    }

}