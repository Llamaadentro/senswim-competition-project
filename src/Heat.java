import com.mgnt.utils.StringUnicodeEncoderDecoder;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Heat {
    int lanes;
    String stroke;
    int distance;
    String supereventname;
    String heatname;
    int heatnum;
    Date heatstarttime;
    Date heatfintime;
    ArrayList<Swimmer> heatparticipants;

    public Heat(String event, int num, Destination destination){
        this.heatnum = num;
        this.heatname = event+ ": heat "+num;
        this.lanes = destination.lanes;
        this.heatparticipants = new ArrayList<Swimmer>();

    }
    void printheatparticipants(){
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        System.out.println(Colors.YELLOW_BRIGHT+this.heatname+Colors.RESET + df.format(heatstarttime));
        for (int i = 0; i < heatparticipants.size(); i++) {

           if(heatparticipants.get(i)==null){
               System.out.println(Colors.CYAN_BOLD_BRIGHT + "Lane " + (i + 1) + ": " + Colors.RESET + "-------------");
           }else {
               System.out.print (Colors.CYAN_BOLD_BRIGHT + "Lane " + (i + 1) + ": " + Colors.RESET);

               String input = heatparticipants.get(i).familyname;
               String res1 = StringUnicodeEncoderDecoder.encodeStringToUnicodeSequence(input);
               String res2 = StringUnicodeEncoderDecoder.decodeUnicodeSequenceToString(res1);

               System.out.print (heatparticipants.get(i).name + " " + res2 + ", ");
               System.out.println( heatparticipants.get(i).birthyear  );
           }

        }
    }
}