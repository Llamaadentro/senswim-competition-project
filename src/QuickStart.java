import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
//��� ������ � ������� � �������
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import java.security.GeneralSecurityException;

//��� ������
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;

import com.google.api.services.drive.model.FileList;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.*;
import com.google.api.services.sheets.v4.Sheets;
import com.itextpdf.text.DocumentException;
import jxl.write.WriteException;

//��� �����
import javax.sound.sampled.AudioInputStream;

import marytts.LocalMaryInterface;
import marytts.exceptions.MaryConfigurationException;
import marytts.exceptions.SynthesisException;
import marytts.signalproc.process.Whisperiser;
import marytts.util.data.audio.MaryAudioUtils;
import marytts.MaryInterface;
import marytts.util.data.audio.AudioPlayer;

//�������������
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine.Info;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;
import static javax.sound.sampled.AudioSystem.getAudioInputStream;
import static javax.sound.sampled.AudioFormat.Encoding.PCM_SIGNED;

//��� JSON
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


import java.io.*;
import java.text.ParseException;
import java.util.*;

public class QuickStart {
    /**     * Application name.     */
    private static final String APPLICATION_NAME = "Scoreloader";

    /**     * Directory to store user credentials for this application.     */
    private static final java.io.File DATA_STORE_DIR = new java.io.File(
            System.getProperty("user.home/"), ".credentials/sheets.googleapis.com-java-quickstart");
    /**     * Global instance of the {@link FileDataStoreFactory}.     */
    private static FileDataStoreFactory DATA_STORE_FACTORY;

    /**     * Global instance of the JSON factory.     */
    private static final JsonFactory JSON_FACTORY =
            JacksonFactory.getDefaultInstance();

    /**     * Global instance of the HTTP transport.     */
    private static HttpTransport HTTP_TRANSPORT;

    /**     * Global instance of the scopes required by this quickstart.
     * <p>
     * If modifying these scopes, delete your previously saved credentials
     * at ~/.credentials/sheets.googleapis.com-java-quickstart     */
    private static final List<String> SCOPESsheet = Arrays.asList(SheetsScopes.SPREADSHEETS, DriveScopes.DRIVE);
    
//    private static final java.util.Collection<String> SCOPES = DriveScopes.all();

    static {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }

    /**     * Creates an authorized Credential object.     * @return an authorized Credential object.     * @throws IOException     */
    public static Credential authorizedrive() throws IOException {
// Load client secrets.
        InputStream in =   QuickStart.class.getResourceAsStream("client_secret2.json");
        GoogleClientSecrets clientSecrets =
                GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

// Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow =
                new GoogleAuthorizationCodeFlow.Builder(
                        HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPESsheet)
                        .setDataStoreFactory(DATA_STORE_FACTORY)
                        .setAccessType("offline")
                        .build();
        Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
        System.out.println(
                "Credentials saved to " + DATA_STORE_DIR.getAbsolutePath());
        return credential;
    }
    
    public static Credential authorizesheet() throws IOException {
// Load client secrets.
        InputStream in =   QuickStart.class.getResourceAsStream("client_secret2.json");
        GoogleClientSecrets clientSecrets =
                GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

// Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow =
                new GoogleAuthorizationCodeFlow.Builder(
                        HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPESsheet)
                        .setDataStoreFactory(DATA_STORE_FACTORY)
                        .setAccessType("offline")
                        .build();
        Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
        System.out.println(
                "Credentials saved to " + DATA_STORE_DIR.getAbsolutePath());
        return credential;
    }
    
    /**     * Build and return an authorized Sheets API client service.     * @return an authorized Sheets API client service     * @throws IOException     */

    public static Sheets getSheetsService() throws IOException {
        Credential credential = authorizesheet();
        return new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    public static Drive getDriveService() throws IOException {
        Credential credential = authorizedrive();
        return new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME).build();
    }
    public static Destination servdestination = null;

    public static void main(String[] args) throws IOException, WriteException, DocumentException, ParseException {

// Build a new authorized API client service.
        Sheets service = getSheetsService();
// https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
//       String spreadsheetId = "1QOwgn_uGSpP83HGG1ZgvOjKn7UXOTNLqnQj3ReqWsV8";

        Drive servicedrive = getDriveService();
        FileList result = servicedrive.files().list().execute();
        
        List<com.google.api.services.drive.model.File> files = result.getFiles();
        if (files == null || files.size() == 0) {
            System.out.println("No files found.");
        } else {

            for (com.google.api.services.drive.model.File file : files) {

                String fname = file.getName();
                System.out.print(fname+":    ");
                String urllink = file.getId();
                System.out.println("https://drive.google.com/file/d/"+urllink);

                String ff = file.getMimeType();
                List<String> parents = file.getParents();

//                System.out.println("https://drive.google.com/file/d/"+urllink);
            }
        }

        String folderId = "1GpWNM08mFZzj8_NZbi_wOv6hT4zOkpNE";
        File fileMetadata = new File();
        fileMetadata.setName("timetable.pdf");
        fileMetadata.setParents(Collections.singletonList(folderId));
        java.io.File filePath = new java.io.File("timetable.pdf");
        FileContent mediaContent = new FileContent("timetable/pdf", filePath);
        File file = servicedrive.files().create(fileMetadata, mediaContent)
                .setFields("id, parents")
                .execute();
        
        System.out.println("File ID: " + file.getId());
        FileList data = servicedrive.files().list().execute();



        boolean checker = false;
        while (!checker) {
        	
        	
            System.out.println(Colors.YELLOW_BRIGHT+"Choose data source(spreadsheet/external source):"+Colors.RESET);
            Scanner inputscanner = new Scanner(System.in);
            String source = inputscanner.nextLine();

            if (source.equals("spreadsheet")) {
                try {
                    String spreadsheetId = "1z5Gu8qu6qE63giwbOSaWFgQnX3Gah0atodUxcUHQAf8";
                    String range = "Verifier!H3:BA";
                    ValueRange response = service.spreadsheets().values().get(spreadsheetId, range).execute();
                    List<List<Object>> values = response.getValues();

                    //������ ���������� � �������

                    Destination TAUC = new Destination(spreadsheetId);
                    TAUC.printdestination();
                    getswimmersdb(spreadsheetId, TAUC);
                    createtimetable(spreadsheetId, TAUC);
                    TAUC.formrealeventlist(servicedrive);
                    TAUC.settimealignment();
                    TAUC.printtimetable();
                    TAUC.createreport(servicedrive);
                    checker = true;
                } catch (Exception e) {
                    e.printStackTrace();
//                    System.out.println("Wrong input data.");
                }
            } else if (source.equals("external source")) {
                try {
                    Destination TAUC = new Destination("New competition", "28.02.2019", "Tel Aviv University Complex", "10:00:00", 50, 10, "");
                    TAUC.settimealignment();
                    TAUC.printtimetable();
                    TAUC.createreport(servicedrive);
                    checker = true;
                } catch (Exception e) {
                    System.out.println("Wrong input data.");
                }
            } else {
                System.out.println(Colors.PURPLE_BRIGHT+"Wrong input option. Choose spreadsheet or external source"+Colors.RESET);
            }

            
            
            
        }
        System.out.println("done");
    }
    //�������� ���� ������ ���� �������

    public static void getswimmersdb (String spreadsheetID, Destination destination) throws IOException {

        Sheets service = getSheetsService();
        ValueRange response = service.spreadsheets().values().get(spreadsheetID, "totaldb!B4:CX").execute();
        List<List<Object>> values = response.getValues();
        if (values == null || values.size() == 0) {
            System.out.println("No data found.");
        } else {
            final TreeMap<String, Swimmer> swimmerdb = new TreeMap<>();

            String[] strokelist = new String[]{
            "Backstroke50","Backstroke100","Backstroke200",
            "Breaststroke50","Breaststroke100","Breaststroke200",
            "Butterfly50","Butterfly100","Butterfly200",
            "Freestyle50","Freestyle100","Freestyle200","Freestyle400","Freestyle800","Freestyle1500",
            "Medley100","Medley200","Medley400", "Relay200","Relay400","Relay800"
            };

            Voice Llamavoice = new Voice("cmu-rms-hsmm");
            for (List row : values) {
              Swimmer guy = new Swimmer(destination,(String) row.get(1),(String) row.get(2),(String) row.get(3),(String) row.get(4));
              guy.phone = (String)row.get(6)+(String)row.get(7);
              guy.email = (String)row.get(8);
              guy.team = (String)row.get(9);
              guy.coach = (String)row.get(10);
              guy.photolink = (String)row.get(11);
              //������� �����
                    Llamavoice.say(guy.name+" "+guy.familyname, (String)row.get(0));

              if(!((String)row.get(12)).equals("")) {
                  guy.height = Integer.parseInt((String) row.get(12));
              } else {
                  guy.height = 0;
              }
              if(!((String)row.get(13)).equals("")) {
              guy.foot = Integer.parseInt((String)row.get(13));
              } else {
                  guy.foot = 0;
              }
              if(!((String)row.get(14)).equals("")) {
              guy.armspan = Integer.parseInt((String)row.get(14));
              } else {
                  guy.armspan = 0;
              }
              if(!((String)row.get(15)).equals("")) {
              guy.weight = Integer.parseInt((String)row.get(15));
              } else {
                  guy.weight = 0;
              }
                for (int i = 16; i <100 ; i+=4) {
                    if(((String)row.get(i)).equals("")){
                        guy.recordlist.put(strokelist[i / 4 - 4], new Record("999","59","99","01/01/2019"));
                    } else {
                        guy.recordlist.put(strokelist[i / 4 - 4], new Record((String) row.get(i), (String) row.get(i + 1), (String) row.get(i + 2), (String) row.get(i + 3)));
                    }
                }
                destination.swimmerdb.put((String)row.get(0), guy);
            }
        }

        ArrayList<Swimmer> swimmers = new ArrayList<>();
        for (String key: destination.swimmerdb.keySet()
             ) {
            swimmers.add(destination.swimmerdb.get(key));
        }
    }

        //�������� ����� ����� ������� �����

    public static void createtimetable (String spreadsheetID, Destination destination) throws IOException, WriteException {

            Sheets service = getSheetsService();
            ValueRange response = service.spreadsheets().values().get(spreadsheetID, "Verifier!I3:BA").execute();
            List<List<Object>> values = response.getValues();

            if (values == null || values.size() == 0) {
            System.out.println("No data found.");
            } else {
                int rowcounter = 0;
                for (List row : values) {
                    if ( !(((String) row.get(1)).equals("-")) && !(((String) row.get(1)).equals(""))){

                           if(rowcounter==0){
                               int colcounter = 7;
                               while (colcounter < row.size()) {
                                   Event event = new Event();
                                   event.stroke = (String) row.get(colcounter);
                                   destination.fulleventlist.add(event);
                                   colcounter++;
                               }

                           } else if (rowcounter==1) {
                               int colcounter = 7;
                               while (colcounter < row.size()) {
                                   destination.fulleventlist.get((colcounter - 7)).distance = Integer.parseInt((String) row.get(colcounter));
                                   colcounter++;
                               }
                           } else if (rowcounter==2) {
                               int colcounter = 7;
                               while (colcounter < row.size()) {
                                   destination.fulleventlist.get((colcounter - 7)).gender = (String) row.get(colcounter);
                                   colcounter++;
                               }
                           } else if (rowcounter==3) {
                               int colcounter = 7;
                               while (colcounter < row.size()) {
                                   destination.fulleventlist.get((colcounter - 7)).age = (String) row.get(colcounter);
                                   colcounter++;
                               }
                           } else if (rowcounter==5) {
                               int colcounter = 7;
                               while (colcounter < row.size()) {
                                   destination.fulleventlist.get((colcounter - 7)).eventorder =  Integer.parseInt((String) row.get(colcounter));
                                   colcounter++;
                               }
                           } else if ( rowcounter>5){
                           Swimmer guy = destination.swimmerdb.get((String)row.get(1)+(String)row.get(2));

                           for (int i = 7 ; i < values.get(0).size(); i++) {
                               if(((String)row.get(i)).equals("TRUE")){
                                  destination.fulleventlist.get(i-7).eventparticipants.add(guy);

//                                     if(destination.fulleventlist.get(i-6).heatquantity==0){
//                                       destination.fulleventlist.get(i-6).heatquantity = 1;
//                                       destination.fulleventlist.get(i-6).heatlist.add(new Heat(destination.fulleventlist.get(i-6).eventname,destination.fulleventlist.get(i-6).heatquantity,destination ));
//                                       destination.fulleventlist.get(i-6).heatlist.get(0).heatparticipants.add(guy);
//                                       } else if (destination.fulleventlist.get(i-6).heatquantity>0 &&
//                                             destination.fulleventlist.get(i-6).heatlist.get(destination.fulleventlist.get(i-6).heatquantity-1).heatparticipants.size()==destination.lanes) {
//                                         destination.fulleventlist.get(i - 6).heatquantity++;
//                                         destination.fulleventlist.get(i - 6).heatlist.add(new Heat(destination.fulleventlist.get(i - 6).eventname, destination.fulleventlist.get(i - 6).heatquantity, destination));
//                                         destination.fulleventlist.get(i - 6).heatlist.get(destination.fulleventlist.get(i - 6).heatquantity - 1).heatparticipants.add(guy);
//                                     } else {
//                                         destination.fulleventlist.get(i - 6).heatlist.get(destination.fulleventlist.get(i - 6).heatquantity - 1).heatparticipants.add(guy);
//                                     }
                               }

                           }
                        }

                    }
                rowcounter++;
                }
            }
    }

//��� ����� �� ������ ������ ��� �������� � ��������
//    public static void insertdata(Sheets service, Swimmer guy, int line) throws IOException {
//
//        List<Object> excellist = new ArrayList<Object>();
//        excellist.add(0, guy.name);
//        excellist.add(1, guy.familyname);
//        excellist.add(2, guy.birthstring);
//        excellist.add(3, guy.age);
//        excellist.add(4, "Gal Nevo");
//        excellist.addAll(guy.events);
//
//        List<List<Object>> newdata = Arrays.asList(excellist);
//        ValueRange body = new ValueRange().setValues(newdata);
//        //UpdateValuesResponse result =
//                service.spreadsheets().values().update("1QOwgn_uGSpP83HGG1ZgvOjKn7UXOTNLqnQj3ReqWsV8", "Participants full!B"+line, body)
//                        .setValueInputOption("RAW")
//                        .execute();
//    }

//    public static void cleareventlist(Sheets service) throws IOException {
////        ValueRange response1 = service.spreadsheets().values().get("1QOwgn_uGSpP83HGG1ZgvOjKn7UXOTNLqnQj3ReqWsV8", "Participants full!G9:AC").execute();
////        List<List<Object>> values1 = response1.getValues();
////        int rowcounter = 0;
////        for (List row : values1) {
////            if (!row.get(0).equals("")) {
////                rowcounter++;
////            }
////        }
//        List<Object> excellist = new ArrayList<Object>();
//        for (int i = 0; i <21 ; i++) {
//            excellist.add(false);
//        }
//
//        List<List<Object>> newdata = Arrays.asList(
//                excellist,excellist,excellist,excellist,excellist
//        );
//        ValueRange body = new ValueRange().setValues(newdata);
//        service.spreadsheets().values().update("1QOwgn_uGSpP83HGG1ZgvOjKn7UXOTNLqnQj3ReqWsV8", "Participants full!G9", body)
//                .setValueInputOption("RAW")
//                .execute();
//    }






}