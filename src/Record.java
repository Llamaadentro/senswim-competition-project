import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Record{
    private String stroke;
    private int distance;

    int recordcs;
    int rmm;
    int rss;
    int rcc;
    String recordstring;
    String datestring;

    public Record( String min, String sec, String csec, String datestring) {

        this.rmm = Integer.parseInt(min);
        this.rss = Integer.parseInt(sec);
        this.rcc = Integer.parseInt(csec);
        this.datestring = datestring;

        this.recordstring = rmm + ":" + rss + ":" + rcc;
        this.recordcs = rcc + rss * 100 + rmm * 6000;
    }

    public Record(int recordcs, String datestring) {
   
        int rmm = (int)(recordcs/6000);
        int rss = (int) ((recordcs-6000*rmm) /100);
        int rcc = (int) ((recordcs-6000*rmm) %100);
        
        this.recordcs = recordcs;
        this.datestring = datestring;
        this.recordstring = rmm + ":" + rss + ":" + rcc;

    }

    public String showrecord(){
        String toshow = this.datestring+": "+this.stroke+", "+this.distance+"m: "+this.recordstring;
        return toshow;
    }

}
