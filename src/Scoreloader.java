
import static org.junit.Assert.assertNotNull;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.TreeMap;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.corundumstudio.socketio.listener.*;
import com.corundumstudio.socketio.*;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import jxl.write.WriteException;

import org.apache.xmlbeans.impl.jam.mutable.MMember;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.omg.CORBA.PUBLIC_MEMBER;

import java.util.Properties;
import javax.mail.*;
import javax.mail.Transport;
import javax.mail.internet.*; 

@Path("/")
public class Scoreloader {
	public static ChatObject chatter = new ChatObject("tester", "testmessage");
	String absolute = "C:/eclipse-workspace/Scoreloader/";
	
	public static String mail = "llamaadentro@gmail.com";
	public static String password = "ignisinanimoNZ1";
	public static String topic = "competition results";	
	
	public static SocketIOServer servercommon = null;
	public static boolean socketisopen = false;
	public static Destination TAUC = null;
	public static Sheets service = null;
	public static Drive servicedrive = null;
	
	public static int filenum = 1;
	
	public static int eventcount = -1;
	public static int heatcount = -1;
	public static int lanecount = 0;
	
	public static ArrayList<Swimmer> eventguys = new ArrayList<Swimmer>();
	public static ArrayList<Swimmer> heatguys = new ArrayList<Swimmer>();
	public static int firstswimmer = -1;
	public static int realswimmers = 0;
	public static int previousstack = 0;
	
    private static final String APPLICATION_NAME = "Scoreloader";
    private static final java.io.File DATA_STORE_DIR = new java.io.File(System.getProperty("user.home/"), ".credentials/sheets.googleapis.com-java-quickstart");
    private static FileDataStoreFactory DATA_STORE_FACTORY;
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static HttpTransport HTTP_TRANSPORT;
    private static final List<String> SCOPESsheet = Arrays.asList(SheetsScopes.SPREADSHEETS, DriveScopes.DRIVE);
//    private static final java.util.Collection<String> SCOPESdrive = DriveScopes.all();

    static {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }

    public static Credential authorizedrive() throws IOException {

        InputStream in =   Scoreloader.class.getResourceAsStream("client_secret2.json");
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                        HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPESsheet)
                        .setDataStoreFactory(DATA_STORE_FACTORY)
                        .setAccessType("offline")
                        .build();
        Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
        System.out.println("Credentials saved to " + DATA_STORE_DIR.getAbsolutePath());
        return credential;
    }
    
    public static Credential authorizesheet() throws IOException {

        InputStream in =   Scoreloader.class.getResourceAsStream("client_secret2.json");
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                        HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPESsheet)
                        .setDataStoreFactory(DATA_STORE_FACTORY)
                        .setAccessType("offline")
                        .build();
        Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
        System.out.println(
                "Credentials saved to " + DATA_STORE_DIR.getAbsolutePath());
        return credential;
    }

    public static Sheets getSheetsService() throws IOException {
        Credential credential = authorizesheet();
        return new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME).build();
    }

    public static Drive getDriveService() throws IOException {
        Credential credential = authorizedrive();
        return new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME).build();
    }
	
	synchronized public static void sendresult(int timenumeric, String servmessage, int number) {
		heatguys.get(number-firstswimmer).currentresult = timenumeric;
		heatguys.get(number-firstswimmer).currentresultstring = servmessage;
		eventguys.get(number-firstswimmer+previousstack).currentresult = timenumeric;
		eventguys.get(number-firstswimmer+previousstack).currentresultstring = servmessage;

		servercommon.getBroadcastOperations().sendEvent("chatevent", new SwimObject(timenumeric,servmessage, ""+number));	
	}
	
	public static void getswimmersdb (String spreadsheetID) throws IOException {
 
	        Sheets service = getSheetsService();
	        ValueRange response = service.spreadsheets().values().get(spreadsheetID, "totaldb!B4:CX").execute();
	        List<List<Object>> values = response.getValues();
	        if (values == null || values.size() == 0) {
	            System.out.println("No data found.");
	        } else {
	            final TreeMap<String, Swimmer> swimmerdb = new TreeMap<>();

	            String[] strokelist = new String[]{
	            "Backstroke50","Backstroke100","Backstroke200",
	            "Breaststroke50","Breaststroke100","Breaststroke200",
	            "Butterfly50","Butterfly100","Butterfly200",
	            "Freestyle50","Freestyle100","Freestyle200","Freestyle400","Freestyle800","Freestyle1500",
	            "Medley100","Medley200","Medley400", "Relay200","Relay400","Relay800"
	            };

	            Voice Llamavoice = new Voice("dfki-poppy-hsmm");
//	            Voice Llamavoice = new Voice("cmu-rms-hsmm");
	            for (List row : values) {
	              Swimmer guy = new Swimmer(TAUC,(String) row.get(1),(String) row.get(2),(String) row.get(3),(String) row.get(4));
	              guy.phone = (String)row.get(6)+(String)row.get(7);
	              guy.email = (String)row.get(8);
	              guy.team = (String)row.get(9);
	              guy.coach = (String)row.get(10);
	              guy.photolink = (String)row.get(11);
	             
	                    Llamavoice.say(guy.name+" "+guy.familyname, (String)row.get(0));
	                    
	                    String folderId = "1GpWNM08mFZzj8_NZbi_wOv6hT4zOkpNE";
	                    File fileMetadata = new File();
	                    fileMetadata.setName(guy.name+guy.familyname+".wav");
	                    fileMetadata.setParents(Collections.singletonList(folderId));
	                    java.io.File filePath = new java.io.File(guy.name+guy.familyname+".wav");
	                    FileContent mediaContent = new FileContent(guy.name+guy.familyname+"/wav", filePath);
	                    File file = servicedrive.files().create(fileMetadata, mediaContent).setFields("id, parents").execute();
	                    
	                    guy.nameaudioadress= file.getId();
	                    FileList data = servicedrive.files().list().execute();
	                    

	              if(!((String)row.get(12)).equals("")) {
	                  guy.height = Integer.parseInt((String) row.get(12));
	              } else {
	                  guy.height = 0;
	              }
	              if(!((String)row.get(13)).equals("")) {
	              guy.foot = Integer.parseInt((String)row.get(13));
	              } else {
	                  guy.foot = 0;
	              }
	              if(!((String)row.get(14)).equals("")) {
	              guy.armspan = Integer.parseInt((String)row.get(14));
	              } else {
	                  guy.armspan = 0;
	              }
	              if(!((String)row.get(15)).equals("")) {
	              guy.weight = Integer.parseInt((String)row.get(15));
	              } else {
	                  guy.weight = 0;
	              }
	                for (int i = 16; i <100 ; i+=4) {
	                    if(((String)row.get(i)).equals("")){
	                        guy.recordlist.put(strokelist[i / 4 - 4], new Record("999","59","99","01/01/2019"));
	                    } else {
	                        guy.recordlist.put(strokelist[i / 4 - 4], new Record((String) row.get(i), (String) row.get(i + 1), (String) row.get(i + 2), (String) row.get(i + 3)));
	                    }
	                }
	                TAUC.swimmerdb.put((String)row.get(0), guy);
	            }
	        }

	        ArrayList<Swimmer> swimmers = new ArrayList<>();
	        for (String key: TAUC.swimmerdb.keySet()
	             ) {
	            swimmers.add(TAUC.swimmerdb.get(key));
	        }
	    }
	
	public static void createtimetable (String spreadsheetID) throws IOException, WriteException {

        Sheets service = getSheetsService();
        ValueRange response = service.spreadsheets().values().get(spreadsheetID, "Verifier!I3:BA").execute();
        List<List<Object>> values = response.getValues();

        if (values == null || values.size() == 0) {
        System.out.println("No data found.");
        } else {
            int rowcounter = 0;
            for (List row : values) {
                if ( !(((String) row.get(1)).equals("-")) && !(((String) row.get(1)).equals(""))){

                       if(rowcounter==0){
                           int colcounter = 7;
                           while (colcounter < row.size()) {
                               Event event = new Event();
                               event.stroke = (String) row.get(colcounter);
                               TAUC.fulleventlist.add(event);
                               colcounter++;
                           }

                       } else if (rowcounter==1) {
                           int colcounter = 7;
                           while (colcounter < row.size()) {
                        	   TAUC.fulleventlist.get((colcounter - 7)).distance = Integer.parseInt((String) row.get(colcounter));
                               colcounter++;
                           }
                       } else if (rowcounter==2) {
                           int colcounter = 7;
                           while (colcounter < row.size()) {
                        	   TAUC.fulleventlist.get((colcounter - 7)).gender = (String) row.get(colcounter);
                               colcounter++;
                           }
                       } else if (rowcounter==3) {
                           int colcounter = 7;
                           while (colcounter < row.size()) {
                        	   TAUC.fulleventlist.get((colcounter - 7)).age = (String) row.get(colcounter);
                               colcounter++;
                           }
                       } else if (rowcounter==5) {
                           int colcounter = 7;
                           while (colcounter < row.size()) {
                        	   TAUC.fulleventlist.get((colcounter - 7)).eventorder =  Integer.parseInt((String) row.get(colcounter));
                               colcounter++;
                           }
                       } else if ( rowcounter>5){
                       Swimmer guy = TAUC.swimmerdb.get((String)row.get(1)+(String)row.get(2));

                       for (int i = 7 ; i < values.get(0).size(); i++) {
                           if(((String)row.get(i)).equals("TRUE")){
                        	   TAUC.fulleventlist.get(i-7).eventparticipants.add(guy);

                           }

                       }
                    }

                }
            rowcounter++;
            }
        }
}
	
	
//																	����� ���������� ���-������
	
//	@Path("/activate")
//	@POST
//	public Response activate() throws InterruptedException {
//
//        Configuration config = new Configuration();
//        config.setHostname("localhost");
//        config.setPort(9092);
//
//        final SocketIOServer server = new SocketIOServer(config);
//        server.addEventListener("chatevent", ChatObject.class, new DataListener<ChatObject>() {
//            @Override
//            public void onData(SocketIOClient client, ChatObject data, AckRequest ackRequest) {
//                // broadcast messages to all clients
//                server.getBroadcastOperations().sendEvent("chatevent", data);
//            }
//           
//        });
//
//        server.start();
//        
//        
//        boolean checker = true;
//        Scanner scanner = new Scanner(System.in);
//        
//        while(checker){
//            String servmessage = scanner.nextLine();
//            server.getBroadcastOperations().sendEvent("serverevent", new ChatObject("Server",servmessage));
//            if(servmessage.equals("quit")){
//                checker = false;
//            }
//        }
//        Thread.sleep(Integer.MAX_VALUE);
//        server.stop();
//        
//        try {
//			String clubInfo = "{\"clubName\":\"The Parks\",\"headCoach\":\"2\",\"coach\":\"1\"}";
//			String newstring = "Server is running";
//			return Response.ok(newstring, MediaType.TEXT_PLAIN).build();
//		}
//		catch(Error e) {
//			e.printStackTrace();
//	        return Response.serverError().entity("error in server").build();
//		}
//        
//    }
		
	@Path("/myservice")
	@GET
	public Response myservice() throws IOException {
		
		 ArrayList<String[]> resultlist = new ArrayList<>();

	        try  {
	            FileInputStream results = new FileInputStream(absolute+"results.txt");
	            BufferedReader resultreader = new BufferedReader(new InputStreamReader(results, "ISO-8859-8"));
	            Scanner swimscan = new Scanner(resultreader);
	            
	            while (swimscan.hasNextLine()){
	                String[] data = swimscan.nextLine().split(":");
	                System.out.println(data[0] +" finished in "+ data[1]);
	                resultlist.add(data);
	            }
	        } catch (Exception ex){
	            ex.getMessage();
	        }
		
	        JSONObject resultjson = new JSONObject();

	        JSONArray swimmers = new JSONArray();
	        
	        for (int i = 0; i < resultlist.size(); i++) {
	            JSONObject record = new JSONObject();

	            record.put("Name", resultlist.get(i)[0]);
	            record.put("Time", resultlist.get(i)[1]);
	                swimmers.add( record);
	        }
	        resultjson.put("Participants", resultlist.size());
	        resultjson.put("Recordlist", swimmers);

	        String finale = resultjson.toJSONString();
	        
	        try(FileWriter file = new FileWriter("file1.txt")){
	            file.write(finale);
	            System.out.println("Object: "+ resultjson);
	        }
		
		System.out.println("it works");
		
//	String fall = "{\"Recordlist\":[{\"Time\":\"01.21.05\",\"Name\":\"Dmitrii Fomin\"},{\"Time\":\"01.22.44\",\"Name\":\"Vladimir Kiselev\"},{\"Time\":\"01.24.32\",\"Name\":\"Igor Azarov\"}],\"Participants\":3}";
			return Response.ok(finale, MediaType.APPLICATION_JSON).build();
				
			
	}
	
	@Path("/postservice")
	@POST
	public Response getClubInfo(String clubId) {
		
		System.out.println("in WorkoutManager.getClubInfo(" + clubId + ")");
			
			try {
				String newstring = chatter.getMessage();
				return Response.ok(newstring, MediaType.TEXT_PLAIN).build();
			}
			catch(Error e) {
				e.printStackTrace();
		        return Response.serverError().entity("error in server").build();
			}
		
	}
	
	@Path("/nextevent")
	@POST
	public Response nextevent() {
		
		String newstring = "";
		
		if(TAUC!=null) {
			eventcount++; 
			heatcount= (-1);	
			
			realswimmers = 0;
			eventguys.clear();
			previousstack = 0;
			
		String age = TAUC.eventmap.get(eventcount).age;
		String gender = TAUC.eventmap.get(eventcount).gender;
		String stroke = TAUC.eventmap.get(eventcount).stroke;
		int dist = TAUC.eventmap.get(eventcount).distance;
		String audio = TAUC.eventmap.get(eventcount).audioadress;

				servercommon.getBroadcastOperations().sendEvent("eventevent", new EventObject(stroke,gender,age,dist,eventcount,audio ));	
				 newstring = ""+TAUC.eventmap.get(eventcount).heatquantity;
				
		}	 else {
				servercommon.getBroadcastOperations().sendEvent("audioevent", new SwimObject(0,"am", "bread"));	
				 newstring = "no destination";
		}
		
			try {
		        return Response.ok(newstring, MediaType.TEXT_PLAIN).build();
			}
			catch(Error e) {
				e.printStackTrace();
		        return Response.serverError().entity("error in server").build();
			}
		
	}
	
	@Path("/nextheat")
	@POST
	public Response nextheat() {
				
//				servercommon.getBroadcastOperations().sendEvent("swimmerevent", new SwimObject());	
		heatcount++;
		
		String heatjsonstring = "";		 
						
		Heat heat = TAUC.eventmap.get(eventcount).heatlist.get(heatcount);
		
		JSONObject heatjson = new JSONObject();
		heatguys.clear();
		previousstack +=realswimmers;
		realswimmers = 0;
		firstswimmer=(-1);
				
		for (int i = 0; i < heat.heatparticipants.size(); i++) {
			if(heat.heatparticipants.get(i)!=null) {
			realswimmers++;	
			if(firstswimmer==-1) {
				firstswimmer = i;
				}
			}
		}
		heatjson.put("participants", realswimmers);
		heatjson.put("firstswimmer",firstswimmer);
		
        JSONArray guys = new JSONArray();
       			        
        for (int i = 0; i < TAUC.lanes; i++) {
        	Swimmer takenguy = heat.heatparticipants.get(i);
           	if(takenguy!=null) {
           		if(takenguy.participated==false) {
           			takenguy.participated = true;
           		}
     
           		heatguys.add(takenguy);
           		eventguys.add(takenguy);
           		
        		JSONObject guy = new JSONObject();

            guy.put("Name",takenguy.name+" "+takenguy.familyname);
            guy.put("Age",takenguy.age);
            guy.put("Audiocall",takenguy.nameaudioadress);
            guy.put("Team",takenguy.team);
            guy.put("Photolink",takenguy.photolink);
            guy.put("Currentrecord", takenguy.recordlist.get(TAUC.eventmap.get(eventcount).stroke + TAUC.eventmap.get(eventcount).distance).recordstring);
            guy.put("Currentrecordnumeric", takenguy.recordlist.get(TAUC.eventmap.get(eventcount).stroke + TAUC.eventmap.get(eventcount).distance).recordcs);
            guy.put("Lane",i);
            	guys.add( guy);
        	}
        }
        heatjson.put("participantslist", guys);

        heatjsonstring = heatjson.toJSONString();
		servercommon.getBroadcastOperations().sendEvent("heatevent", new HeatObject(heatjsonstring));
		
			try {
		        return Response.ok(heatjsonstring, MediaType.APPLICATION_JSON).build();
			}
			catch(Error e) {
				e.printStackTrace();
		        return Response.serverError().entity("error in server").build();
			}
		
	}
	
	@Path("/nextswimmer")
	@POST
	public Response nextswimmer() {
				
				servercommon.getBroadcastOperations().sendEvent("swimmerevent", new SwimObject());	
				String newstring = "hello swimmer";
 				
		    	
			try {
		        return Response.ok(newstring, MediaType.TEXT_PLAIN).build();
			}
			catch(Error e) {
				e.printStackTrace();
		        return Response.serverError().entity("error in server").build();
			}
		
	}

	@Path("/openscreen")
	@POST
	public Response openscreen() throws InterruptedException {
		
		Configuration config = new Configuration();
        config.setHostname("localhost");
        config.setPort(9092);
        
        servercommon = new SocketIOServer(config);
        servercommon.start();
        socketisopen = true;
        Thread.sleep(2000);
				
		  JSONObject destination = new JSONObject();

		  destination.put("Competitionname", TAUC.competitionname);
		  destination.put("Competitionimage", TAUC.pictureadress);
              

      String finale = destination.toJSONString();
 			
			try {
		        return Response.ok(finale, MediaType.APPLICATION_JSON).build();
			}
			catch(Error e) {
				e.printStackTrace();
		        return Response.serverError().entity("error in server").build();
			}
		
	}
	
	@Path("/closesocket")
	@POST
	public Response closesocket() {
 				
				if(socketisopen) {
 		        servercommon.stop();
		        servercommon = null;
		        socketisopen = false;
		        System.out.println("socket closed");
		        
		    try {
				String newstring = "connection closed";
			    return Response.ok(newstring, MediaType.TEXT_PLAIN).build();
			} catch(Error e) {
				e.printStackTrace();
			    return Response.serverError().entity("error in server").build();
			}
				} else {
					System.out.println("socket already closed");
			try {
				String newstring = "connection already closed";
		        return Response.ok(newstring, MediaType.TEXT_PLAIN).build();
			}
			catch(Error e) {
				e.printStackTrace();
		        return Response.serverError().entity("error in server").build();
			}
				}
	}
	
	@Path("/takemarks")
	@POST
	public Response takemarks() {
				
				servercommon.getBroadcastOperations().sendEvent("marksevent", new SwimObject(0,"am", "bread"));	
				String newstring = "let's go";
 			
			try {
		        return Response.ok(newstring, MediaType.TEXT_PLAIN).build();
			}
			catch(Error e) {
				e.printStackTrace();
		        return Response.serverError().entity("error in server").build();
			}
		
	}
	
	@Path("/print")
	@POST
	public Response print(String newstring) throws InterruptedException {
				
			System.out.println(newstring);
		
 			
			 try( Writer out = new BufferedWriter(new OutputStreamWriter (new FileOutputStream("C:/eclipse-workspace/Scoreloader/file"+filenum+".txt"), "UTF-8"));
//			     BufferedOutputStream bufremains = new BufferedOutputStream(out))
					 )
			        {
			            byte[] buffer = newstring.getBytes();
			            out.write(newstring,0,newstring.length());
			            filenum++;
			            out.close();
			            System.out.println("success!");
			            Thread.sleep(1400);
			            
			           
			        } catch (Exception ex){
			            System.out.println(ex.getMessage());
			        }
			
			try {
		        return Response.ok("hello", MediaType.TEXT_PLAIN).build();
			} catch(Error e) {
				e.printStackTrace();
		        return Response.serverError().entity("error in server").build();
			}
		
	}
	
	@Path("/mailer")
	@POST
	public Response mailparticipants() {
				
				
			for (String key: TAUC.swimmerdb.keySet()
	             ) {
	            Swimmer guy = TAUC.swimmerdb.get(key);
	            if(guy.participated) {
	            	Mailer.send("llamaadentro@gmail.com","ignisinanimoNZ1",guy.email,"Competition results for "+guy.name+" "+guy.familyname,"Dear "+guy.name+"! Thanks for participating!");
	            	
	            }
	        }
		
			try {
		        return Response.ok("Mails sent!", MediaType.TEXT_PLAIN).build();
			}
			catch(Error e) {
				e.printStackTrace();
		        return Response.serverError().entity("error in server").build();
			}
		
	}
	
	
	@Path("/sortresults")
	@POST
	public Response sortresults() {
				
		String resultjsonstring = "";	
				CurrentResultComparator rescomp = new CurrentResultComparator();
				heatguys.sort(rescomp);
		
				JSONObject resultjson = new JSONObject();
				
				JSONArray guys = new JSONArray();
			        
		        for (int i = 0; i < heatguys.size(); i++) {
		        	Swimmer takenguy = heatguys.get(i);
		           	
		        		JSONObject guy = new JSONObject();

		            guy.put("Name",takenguy.name+" "+takenguy.familyname);
		            guy.put("Age",takenguy.age);
		            guy.put("Audiocall",takenguy.nameaudioadress);
		            guy.put("Team",takenguy.team);
		            guy.put("Photolink",takenguy.photolink);
		            guy.put("Currentrecord", takenguy.recordlist.get(TAUC.eventmap.get(eventcount).stroke + TAUC.eventmap.get(eventcount).distance).recordstring);
		            guy.put("Currentrecordnumeric", takenguy.recordlist.get(TAUC.eventmap.get(eventcount).stroke + TAUC.eventmap.get(eventcount).distance).recordcs);
		            guy.put("Timerecord", takenguy.currentresultstring);
		            guy.put("Timerecordnumeric", takenguy.currentresult);
		            guy.put("Lane",i);
		            	guys.add( guy);
		        	
		        }
		        resultjson.put("participantslist", guys);
		        resultjsonstring = resultjson.toJSONString();
		
				servercommon.getBroadcastOperations().sendEvent("sortevent", new HeatObject(resultjsonstring));	
				String newstring = "let's go";
 			
			try {
		        return Response.ok(newstring, MediaType.TEXT_PLAIN).build();
			}
			catch(Error e) {
				e.printStackTrace();
		        return Response.serverError().entity("error in server").build();
			}
		
	}
	
	@Path("/showwinners")
	@POST
	public Response showwinners() {
				
		String resultjsonstring = "";	
				CurrentResultComparator rescomp = new CurrentResultComparator();
				eventguys.sort(rescomp);
		
				JSONObject resultjson = new JSONObject();
				
				JSONArray guys = new JSONArray();
			        
		        for (int i = 0; i < eventguys.size(); i++) {
		        	Swimmer takenguy = eventguys.get(i);
		           	
		        		JSONObject guy = new JSONObject();

		            guy.put("Name",takenguy.name+" "+takenguy.familyname);
		            guy.put("Age",takenguy.age);
		            guy.put("Audiocall",takenguy.nameaudioadress);
		            guy.put("Team",takenguy.team);
		            guy.put("Photolink",takenguy.photolink);
		            guy.put("Currentrecord", takenguy.recordlist.get(TAUC.eventmap.get(eventcount).stroke + TAUC.eventmap.get(eventcount).distance).recordstring);
		            guy.put("Currentrecordnumeric", takenguy.recordlist.get(TAUC.eventmap.get(eventcount).stroke + TAUC.eventmap.get(eventcount).distance).recordcs);
		            guy.put("Timerecord", takenguy.currentresultstring);
		            guy.put("Timerecordnumeric", takenguy.currentresult);
		            guy.put("Lane",i);
		            	guys.add( guy);
		        	
		        }
		        resultjson.put("participantslist", guys);
		        resultjsonstring = resultjson.toJSONString();
		
				servercommon.getBroadcastOperations().sendEvent("podiumevent", new HeatObject(resultjsonstring));	
				String newstring = "let's go";
 			
			try {
		        return Response.ok(newstring, MediaType.TEXT_PLAIN).build();
			}
			catch(Error e) {
				e.printStackTrace();
		        return Response.serverError().entity("error in server").build();
			}
		
	}
	
	
	@Path("/givewhistle")
	@POST
	public Response runevent() throws InterruptedException {     
        
		
		servercommon.getBroadcastOperations().sendEvent("whistleevent", new SwimObject(0,"am", "bread"));	
        Thread.sleep(1400);
       
        Thread[] swimmerthreads = new Thread [realswimmers];
        
        for (int j = firstswimmer; j < realswimmers+firstswimmer; j++) {
			
        Thread swimmer = new Thread(new Simulator(servercommon, TAUC.eventmap.get(eventcount), j));
        swimmerthreads[j-firstswimmer] = swimmer;
        }
        
        for (int k = 0; k < swimmerthreads.length; k++) {
			swimmerthreads[k].start();
		}
        System.out.println("started");
        
        for (int k = 0; k < swimmerthreads.length; k++) {
			swimmerthreads[k].join();
		}
        
//        int checker = 0;
//       
//        while(checker<3){
//        	
//        	
//            String servmessage = "swimmer "+checker;
//            server.getBroadcastOperations().sendEvent("chatevent", new SwimObject("Server",servmessage, "p"+checker));
//            
//            checker++;
//            
//        }
        
//        Thread.sleep(Integer.MAX_VALUE);
//        Thread.sleep(2000);
//        servercommon.getBroadcastOperations().sendEvent("audioevent", new SwimObject("i","am", "bread"));	
//        servercommon.stop();
//        servercommon = null;
        
        try {
			String newstring = "All swimmers have finished";
			return Response.ok(newstring, MediaType.TEXT_PLAIN).build();
		}
		catch(Error e) {
			e.printStackTrace();
	        return Response.serverError().entity("error in server").build();
		}
        
         
    }
	
	@Path("/getcompdata")
	@POST
	public Response getcompdata(String spreadsheetid) {			
			try {
				String newstring = chatter.getMessage();
				 try {
					 	service = getSheetsService();
					 	servicedrive = getDriveService();
					 	
	                    String spreadsheetId = spreadsheetid;
	                   
	                    System.out.println(spreadsheetId);
	                    
//	                    String spreadsheetId = "1z5Gu8qu6qE63giwbOSaWFgQnX3Gah0atodUxcUHQAf8";
	                    String range = "Verifier!H3:BA";
	                    ValueRange response = service.spreadsheets().values().get(spreadsheetId, range).execute();
	                    List<List<Object>> values = response.getValues();

	                    TAUC = new Destination(spreadsheetId);
	                    TAUC.printdestination();
	                    getswimmersdb(spreadsheetId);
	                    createtimetable(spreadsheetId);
	                    TAUC.formrealeventlist(servicedrive);
	                    TAUC.settimealignment();
	                    TAUC.printtimetable();
	                    TAUC.createreport(servicedrive);
	                } catch (Exception e) {
	                    e.printStackTrace();
	                }
					
				 JSONObject destinationjson = new JSONObject();
			        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");

			        destinationjson.put("name", TAUC.competitionname);
			        destinationjson.put("place", TAUC.poolname);
			        destinationjson.put("date", TAUC.date);
			        destinationjson.put("lanes", TAUC.lanes);
			        destinationjson.put("poollength", TAUC.poollength);
			        destinationjson.put("picture", TAUC.pictureadress);
			        destinationjson.put("start",df.format(TAUC.warmupstarttime));
			        destinationjson.put("timetablelink", TAUC.timetablefulllink);
			        
			        JSONArray eventmenu = new JSONArray();
			       			        
			        for (int i = 0; i < TAUC.eventmap.size(); i++) {
			            JSONObject event = new JSONObject();

			            event.put("Eventname", TAUC.eventmap.get(i).eventname);
			            event.put("Eventtime", df.format(TAUC.eventmap.get(i).eventstarttime));
			                eventmenu.add( event);
			        }
			        destinationjson.put("eventlist", eventmenu);

			        String finale = destinationjson.toJSONString();
			        
				 
				return Response.ok(finale, MediaType.APPLICATION_JSON).build();
			}
			catch(Error e) {
				e.printStackTrace();
		        return Response.serverError().entity("error in server").build();
			}	
	}
	
	
	
	
	
}


class ChatObject {

    private String userName;
    private String message;

    public ChatObject() {
    }

    public ChatObject(String userName, String message) {
        super();
        this.userName = userName;
        this.message = message;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

}

class SwimObject {

    private int name;
    private String message;
    private String num;

    public SwimObject() {
    }

    public SwimObject(int userName, String message, String num) {
        super();
        this.name = userName;
        this.message = message;
        this.num = num;
    }

    public int getName() {
        return name;
    }
    public void setName(int userName) {
        this.name = userName;
    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public String getNum() {
        return num;
    }
    public void setNum(String num) {
        this.num = num;
    }

}

class EventObject {

    private String stroke;
    private int distance;
    private int eventnum;
    private String age;
    private String gender;
    private String audio;
    
    public EventObject() {
    }

    public EventObject(String stroke, String gender, String age, int distance, int eventnum,  String audio) {
        super();
        this.stroke = stroke;
        this.gender = gender;
        this.age = age;
        this.distance = distance;
        this.eventnum = eventnum;
        this.audio = audio;
    }

    public String getStroke() {
        return stroke;
    }
    public void setStroke(String stroke) {
        this.stroke = stroke;
    }

    public int getDistance() {
        return distance;
    }
    public void setDistance(int distance) {
        this.distance = distance;
    }
    public int getEventnum() {
        return eventnum;
    }
    public void setEventnum(int num) {
        this.eventnum = num;
    }
    public String getAge() {
        return age;
    }
    public void setAge(String age) {
        this.age = age;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public String getAudio() {
        return audio;
    }
    public void setAudio(String audio) {
        this.audio = audio;
    }
}

class HeatObject {

    private String heatjson;

    public HeatObject() {
    }

    public HeatObject(String heatjson) {
        super();
        this.heatjson = heatjson;
    }

    public String getHeatjson() {
        return heatjson;
    }
    public void setHeatjson(String heatjson) {
        this.heatjson = heatjson;
    }

}

class GuyObject {

    private String heatjson;

    public GuyObject() {
    }

    public GuyObject(String heatjson) {
        super();
        this.heatjson = heatjson;
    }

    public String getHeatjson() {
        return heatjson;
    }
    public void setHeatjson(String heatjson) {
        this.heatjson = heatjson;
    }

}



class Simulator implements Runnable {
	
	SocketIOServer server;
	int number;
	int timenumeric;
	Event event;
	
	int additional;
	int distance;
	
	
	 public Simulator(SocketIOServer server, Event event, int guynum) {
		
		this.number = guynum;
		this.server = server;
		this.event = event;
		this.distance = event.distance;
		
		switch(distance) {
		case 50: additional = 43; break;
		case 100: additional = 95; break;
		case 200: additional = 215; break;
		case 400: additional = 435; break;
		case 800: additional = 900; break;
		default: additional =1680 ;
		}
	}
	
	
	@Override
	public void run() {
		
		Random chow = new Random();
		int mil = 6000;
		int nan = chow.nextInt(400);
		String seczero = "";
		String nanzero = "";
		
	if( (6+additional+(int)(nan/100))%60 < 10) {
		seczero = "0";
	}
	if(nan%100<10) {
		nanzero = "0";
	}
	
		String servmessage = (int) ((6+additional+(int)(nan/100))/60)+":"+seczero+( (6+additional+(int)(nan/100))%60)+":"+nanzero+(nan%100);
		
		this.timenumeric = mil/10 + additional*100 + nan ;
		
		try{
        Thread.sleep(mil+(nan*10));
		} catch (InterruptedException ex){
            System.out.println(ex.getMessage());
        }
//        server.getBroadcastOperations().sendEvent("chatevent", new SwimObject(name,servmessage, "p"+number));
        Scoreloader.sendresult(timenumeric, servmessage, number); 
		
	}
}

class Mailer{
	public static void send(String from,String password,String to,String sub,String msg){
		//Get properties object
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		//get Session
		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(from,password);
					}
				});
		//compose message
		try {
			MimeMessage message = new MimeMessage(session);
			message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));
			message.setSubject(sub);
			message.setText(msg);
			//send message
			Transport.send(message);
			System.out.println("message sent successfully");
		} catch (MessagingException e) {throw new RuntimeException(e);}

	}
}

