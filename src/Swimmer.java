import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.TreeMap;

public class Swimmer {
	 	
	Destination destination;
	    String name;
	    String familyname;
	    LocalDate birthdate;
	    int birthyear;
	    String birthstring;
	    String gender;
	    int age;
	    String phone;
	    String email;
	    String team;
	    String coach;
	    String photolink;
	    String namelink;
	    int height;
	    int foot;
	    int armspan;
	    int weight;
	    TreeMap<String, Record> recordlist;
	    ArrayList<Event> eventstoswim;
	    String nameaudioadress;
	    int currentresult;
	    String currentresultstring;
	    boolean participated;
	    
	    public Swimmer(Destination destination, String name, String familyname, String gender, String birthdate){
	        this.destination = destination;
	        this.name =name;
	        this.familyname = familyname;
	        this.gender = gender;

	        int daycounter =0;
	        int monthcounter =0;
	        for (int i = 0; i < birthdate.length() ; i++) {
	            if (birthdate.substring(i,i+1).equals(".") || birthdate.substring(i,i+1).equals("/")){
	                daycounter = i;
	                break;
	            }
	        }
	        for (int i = daycounter+1; i < birthdate.length() ; i++) {
	            if (birthdate.substring(i,i+1).equals(".") || birthdate.substring(i,i+1).equals("/")){
	                monthcounter = i;
	                break;
	            }
	        }
	        if (birthdate.equals("")) {
	            this.birthdate = LocalDate.of(Integer.parseInt("2000"), Integer.parseInt("01"), Integer.parseInt("01"));
	            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.YYYY");
	            this.birthstring = formatter.format(this.birthdate);
	        } else {
	            this.birthdate = LocalDate.of(Integer.parseInt(birthdate.substring(monthcounter + 1, birthdate.length())), Integer.parseInt(birthdate.substring(daycounter + 1, monthcounter)), Integer.parseInt(birthdate.substring(0, daycounter)));
	            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.YYYY");
	            this.birthstring = formatter.format(this.birthdate);
	        }

	        int basicage = LocalDate.now().getYear() - this.birthdate.getYear();
	        if (LocalDate.now().getMonthValue() < this.birthdate.getMonthValue() || (LocalDate.now().getMonthValue() == this.birthdate.getMonthValue())&&(LocalDate.now().getDayOfMonth() < this.birthdate.getDayOfMonth()) ){
	            this.age = basicage-1;
	        } else {
	            this.age = basicage;
	        }
//	        this.age = Integer.parseInt(age);
	        this.eventstoswim = new ArrayList<Event>();
	        this.recordlist = new TreeMap<String, Record>();
	        this.birthyear = this.birthdate.getYear();
	        this.nameaudioadress = "";
	        this.currentresult = 0;
	        this.participated = false;
	    }

	    public Swimmer(String name, String familyname, String birthyear, String team){
	        this.name = name;
	        this.familyname = familyname;
	        if(birthyear.equals("")){
	            this.birthyear = 2019;
	        } else {
	            this.birthyear = Integer.parseInt(birthyear);
	        }
	        this.team = team;
	        this.recordlist = new TreeMap<String, Record>();
	        this.nameaudioadress = "";
	        this.currentresult = 0;
	        this.participated = false;
	    }


	
	
}
