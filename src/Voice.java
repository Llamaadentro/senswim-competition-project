import marytts.LocalMaryInterface;
import marytts.MaryInterface;
import marytts.exceptions.MaryConfigurationException;
import marytts.exceptions.SynthesisException;
import marytts.signalproc.process.Whisperiser;
import marytts.util.data.audio.AudioPlayer;
import marytts.util.data.audio.MaryAudioUtils;

import javax.sound.sampled.AudioInputStream;
import java.io.IOException;

public class Voice
{
    private MaryInterface marytts;
    AudioPlayer ap;

    public Voice(String voiceName){
        try
        {
            marytts = new LocalMaryInterface();
            marytts.setVoice(voiceName);
            marytts.setAudioEffects("Chorus(delay1:266;amp1:0.54;delay2:600;amp2:-0.10;delay3:250;amp3:0.30)");
//			marytts.setAudioEffects("Whisper(Amount:75)");
//			marytts.setAudioEffects("Robot(Amount:77)+FIRFilter(type:2;fc1:400.0;fc2:900.0)+Volume(Amount:2.0)+Whisper(Amount:10.0)");
            ap = new AudioPlayer();
        }
        catch (MaryConfigurationException ex)
        {
            ex.printStackTrace();
        }
    }

    public void say(String input, String filename)
    {
        try
        {
            AudioInputStream audio = marytts.generateAudio(input);
//			ap.setAudio(audio);
//			ap.start();
//			ap.join();

            double[] samples = MaryAudioUtils.getSamplesAsDoubleArray(audio);
            try {

                MaryAudioUtils.writeWavFile(samples, filename+".wav", audio.getFormat());

                System.out.println("Output written to " + filename+".wav");
            } catch (IOException e) {
                System.err.println("Could not write to file: " + filename+".wav" + "\n" + e.getMessage());
                System.exit(1);
            }
        }
        catch (SynthesisException ex){
            System.err.println("Error saying phrase.");
//		} catch (InterruptedException ex){
//			System.err.println("Error synchronizing");
        }
    }
}